package clui;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class EnrolTest extends BaseTest {
		
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        System.out.println("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("Sheet2");
	}
	
	
	@Test(priority=1)
	public void coursePerform() throws InterruptedException, IOException
	{
		
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				ViewCoursePage viewCoursePage=new ViewCoursePage(driver,wait, action, prop);
				
			
					
				//homepage methods
				driver.get(prop.getProperty("url"));
				int attemptnum =1;
			
				//login steps
				do{
					homePage.goToCredentials();
					
			
					//Loginpage method
					loginPage2.loginQA(ExcelUtil.getRowData(1)); //enter user name and password
					
			
					//wait for overlay to disappear
					loginPage2.loadingCourses();
					Thread.sleep(3000);

					
						if(driver.findElements(profilePage2.myProfileLink()).size()>0) //check if user is logged in or not
						{
							break;
						}
				
				System.out.println(attemptnum + " login attempt failed for user -" + loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login" );
				attemptnum++; // add up the attempts
					
				}while(driver.findElements(By.id("clui-login-button")).size()>0);
					
				//login process end here
				
				//navigate to course page
				coursePage2.courses();
				
				//navigate to all courses tab
				coursePage2.allCourses();
				
				
				String courseName=prop.getProperty("performCourseName"); //get the course name from data file
				
				//search for any course
				coursePage2.searchCourse(courseName);
				
				
				//navigate to edit course page
				coursePage2.viewCourse();
				
				
				SoftAssert softAssert = new SoftAssert();
				
				/* After reaching the view course page, validate whether it has unenrol button or enrol button*/
				if(driver.findElements(viewCoursePage.enrolButtonElement()).size()>0)
					{
						softAssert.assertTrue(true, "enrol button is not visisble ");
						
						//call the validate method to validate the enrol button 
						validateEnrolButton(viewCoursePage.enrolButtonElement());
					
						//enroll in the course  
						viewCoursePage.enrollCourse();
						
						/*Now check the form modal is open or not*/
						if(driver.findElement(viewCoursePage.closeFormModal()).isDisplayed())
							{
								softAssert.assertTrue(true, "Form1 modal is not displayed ");
								System.out.println("in form modal default");
								viewCoursePage.formDetails();
							}
						else if(driver.findElement(viewCoursePage.launchButton()).isDisplayed())
							{
								softAssert.assertTrue(true, "launch button is not displayed ");
								System.out.println("not in form modal");
								viewCoursePage.openModal(viewCoursePage.launchButton()); // open the modal
								viewCoursePage.formDetails();
								
							}
						else
							{
								softAssert.assertTrue(false);
								System.out.println("some error");
							}
					}
				
				
				//click submit form
				viewCoursePage.submitModule();
			
				 
				
				
				
				
					
				
	}
	
	
	public void validateEnrolButton(By enrolButtonElement )
	{
		
			System.out.println("Reached validation point");
			//Check it is clickable and then click on it to enrol
			String enrolButtonClass= driver.findElement(enrolButtonElement).getAttribute("class");
			boolean isDisabled = enrolButtonClass.contains("disabled");
			
			if(isDisabled==true)
			{
				
				Assert.assertTrue(false, "The enrol button is disabled");

			}
			else
			{
				Assert.assertTrue(true);
				System.out.println("The enrol button is not disabled");
			}
			
		
		
		
	}
	

}
