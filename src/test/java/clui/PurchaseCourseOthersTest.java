package clui;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class PurchaseCourseOthersTest extends BaseTest {

	public static Logger log = LogManager.getLogger(PurchaseCourseOthersTest.class.getName());

	@BeforeTest
	public void setupTestData() throws Exception {
		// Set Test Data Excel and Sheet
		log.info("************Setup Test Level Data**********");
		ExcelUtil.setExcelFileSheet("login");

		log.info(
				"**********************Test - Buying course for others and checking the alert message in case of invalid user is added to list****************");
		log.info("Excel sheet- login is intialised");
	}

	@Test(priority = 0)
	public void coursePerform() throws Exception {

		// page instantiation
		HomePage homePage = new HomePage(driver, wait, action, prop);
		LoginPage2 loginPage2 = new LoginPage2(driver, wait, action, prop);
		CoursePage2 coursePage2 = new CoursePage2(driver, wait, action, prop);
		ProfilePage2 profilePage2 = new ProfilePage2(driver, wait, action, prop);
		ViewCoursePage viewCoursePage = new ViewCoursePage(driver, wait, action, prop);
		CourseInsightPage2 courseInsightPage2 = new CourseInsightPage2(driver, wait, action, prop);

		// homepage methods
		driver.get(prop.getProperty("url"));
		int attemptnum = 1;
		boolean loginSuccess = false;
		waitForAjax("Home page");

		// login steps------------------------------------------------------
		do {
			homePage.goToCredentials();

			// wait for page load
			waitForAjax("Login page");

			// Loginpage method
			// enter user name and password
			loginPage2.loginQA(ExcelUtil.getRowData(1));

			log.info("Credential entered");

			// wait for overlay to disappear
			loginPage2.loadingCourses();

			waitForAjax("After entering credentials");

			Thread.sleep(3000);

			// check if user is logged in or not
			if (driver.findElements(profilePage2.myProfileLink()).size() > 0
					|| driver.findElements(loginPage2.userLogoutOption()).size() > 0) {

				loginSuccess = true;
			} else if (driver.findElements(By.id("clui-login-button")).size() > 0) {
				loginSuccess = false;
				log.info(attemptnum + " login attempt failed for user -" + loginPage2.testType(ExcelUtil.getRowData(1))
						+ ". So, will try again to login");
				attemptnum++; // add up the attempts
			} else {
				log.info("The login credential has issues, please check");
				Assert.assertTrue(false);
			}

		} while (!loginSuccess);

		// login process end here------------------------------------------------------

		// navigate to course page
		coursePage2.courses();
		
		
		
		
		
		// enter the course url and purchase the course for others
		
			log.info("--------Enter the courseDetails page url------");
			driver.get(prop.getProperty("paidPerformCourseURL"));
			// wait for the course insight overlay to disappear
			wait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.cssSelector(".section-content .clui-overlay-loading.light")));

			log.info("------Successfully reached course detail page-------");

			SoftAssert softAssert = new SoftAssert();

			waitForAjax("Course details page");

			/*
			 * After reaching the view course page, validate whether it has unenrol button
			 * or enrol button
			 */
			if (driver.findElements(viewCoursePage.purchaseButtonElement()).size() > 0) {
				softAssert.assertTrue(true, "purchase button is not visisble ");
				log.info("inside purchase process ");


				// click on purchase button to enroll and purchase course
				viewCoursePage.clickPurchaseCourseOthers();

				// validate the purchase method and accordingly purchase the course
				String cardType=getCardType(viewCoursePage.selectCardLabel());
				
				log.info("Purchase for other users with valid card details");
				
				//get valid card details	
				String[] cardDetails= getValidCardDetails(prop.getProperty("purchase_for_others_cardType"));

				//purchase the course with valid card details		
				viewCoursePage.purchaseCard(cardType, cardDetails[0], cardDetails[1], cardDetails[2], cardDetails[3],cardDetails[4]  );
						
						
					// wait for the purchase success message to appear
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".box-success")));

					// validate the purchase success message
					validatePurchaseSuccess(viewCoursePage.purchaseSuccessMessage());

					// close the purchase success modal
					viewCoursePage.viewCourseAction();

					/*-------------- Remove the user from the course ---------------*/

					// navigate to course insight page
					viewCoursePage.courseInsight();

					waitForAjax("Course insight page");

					log.info("Reached course insight page");
					
					int rowNum=1;
					while(getValidUser(rowNum)!=null) {
						
	
							//when the user type is invalid 
							if(getValidUser(rowNum).equalsIgnoreCase("invalid user"))
							{		
								log.info("The user is invalid, so check the next user");
								rowNum++;
							}
							else {
							
								log.info("The user is valid- " +getValidUser(rowNum) );
								log.info("So, search this user and unenrol the same" );
								
							//when user type is valid
							// search for that user and unenrol from that course
							courseInsightPage2.searchUser(getValidUser(rowNum));
							courseInsightPage2.removeUser();
							
							// clear the search filter
							courseInsightPage2.clearFilter();
							rowNum++;
							}
							
						}
						
					
					
					}
					
			// logout method
			loginPage2.logoutQA();

			log.info("logged out");
			waitForAjax(" Home page after logout");	
				
	}
	
	
	
	//method to search users and remove from course insight page
	public String  getValidUser(int rowNum) throws Exception
	{
		//Set Test Data Excel and Sheet
		ExcelUtil.setExcelFileSheet("purchaseUserList");
		
	
			
			if(ExcelUtil.getCellData(rowNum, 0)!=null) {
				if(ExcelUtil.getCellData(rowNum, 0).equalsIgnoreCase(prop.getProperty("purchase_for_others_userType")))
				{
					//in case of valid user return username
					String validUser= ExcelUtil.getCellData(rowNum, 1);
					return validUser; 
					
				}
				else
				{
					//incase of invalid user name
					String invalid= "invalid user";
					return invalid;
				}
			}
			else
			{
				//when no row left to check
				return null;
			}
		
		
	}
	
	
	// method for validating the purchase success message
	public void validatePurchaseSuccess(By purchaseSuccessMessage) {
		log.info("Validating the purchase success  message");

		String purchaseSuccess = driver.findElement(purchaseSuccessMessage).getText().trim();
		if (purchaseSuccess.contains("Purchase successful!")) {
			log.info("The purchase success message is -" + purchaseSuccess);
			Assert.assertTrue(true);
			
		} else {
			log.info("The purchase success message is changed" + purchaseSuccess);
			Assert.assertTrue(false);
		}

	}


	public String getCardType(By selectCard) {
		log.info("-------- Validating card selection method------");

		// fetch the selection text and validate the card details for purchase
		String selectCardLabel = driver.findElement(selectCard).getText().trim();

		return selectCardLabel;
	}
	
	public String[] getValidCardDetails(String purchaseType) throws Exception
	{
		//Set Test Data Excel and Sheet
		ExcelUtil.setExcelFileSheet("cardTest");
		
		int rowNum=1;
		while(!ExcelUtil.getCellData(rowNum, 0).equalsIgnoreCase(purchaseType)) {
			rowNum++;
		}
		
		 //Intialise the card details
		String cardHolderName = ExcelUtil.getCellData(rowNum, 3);
		String cardNumber = ExcelUtil.getCellData(rowNum, 4);
		String expMonth = ExcelUtil.getCellData(rowNum, 5);
		String expYear = ExcelUtil.getCellData(rowNum, 6);
		String cvv = ExcelUtil.getCellData(rowNum, 7);
				
		log.info("Card details entering for - " + purchaseType );
		log.info("card holder name- " + cardHolderName + 
				"\n card number - " + cardNumber+ 
				"\n expiry month - " + expMonth + 
				"\n expiry year - " + expYear+
				"\n cvv - " + cvv);
			
		return new String[] {cardHolderName, cardNumber, expMonth, expYear, cvv};
	}

}
