package clui;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class CoursePerformAssessmentTestIE extends BaseTest {
		
	public static Logger log= LogManager.getLogger(CoursePerformAssessmentTestIE.class.getName());
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        log.info("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("login");
        
        log.info("**********************Testing - approval module and assessment module****************");
        log.info("Excel sheet- login is intialised");
	}
	
	
	@Test(priority=0)
	public void coursePerform() throws InterruptedException, IOException
	{
		
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				ViewCoursePage viewCoursePage=new ViewCoursePage(driver,wait, action, prop);
				CourseInsightPage2 courseInsightPage2=new CourseInsightPage2(driver,wait, action,prop);
				EnrolmentInsightPage enrolmentInsightPage=new EnrolmentInsightPage(driver,wait, action,prop);
					
				//homepage methods
				driver.get(prop.getProperty("url"));
				int attemptnum =1;
				boolean loginSuccess=false;
				waitForAjax("Home page");
				
				//log.info("Sleep for 10 sec");
				 //Thread.sleep(10000);
				 
				//login steps
				do{
					homePage.goToCredentials();
					// wait for page load
					waitForAjax("Login page");
			
					//Loginpage method
					//enter user name and password
					loginPage2.loginQA(ExcelUtil.getRowData(3)); 
					
					log.info("Credential entered");
					
					//wait for overlay to disappear
					loginPage2.loadingCourses();
					
					waitForAjax("After entering credentials");
					
					Thread.sleep(3000);

					// check if user is logged in or not
					if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) 
					{
						
						loginSuccess= true;
					}
					else if(driver.findElements(By.id("clui-login-button")).size() > 0)
					{
						loginSuccess= false;
						log.info(attemptnum + " login attempt failed for user -"
								+ loginPage2.testType(ExcelUtil.getRowData(3)) + ". So, will try again to login");
						attemptnum++; // add up the attempts
					}
					else 
					{
						log.info("The login credential has issues, please check");
						Assert.assertTrue(false);
					}
					

				} while (!loginSuccess);

				// login process end here
					
				
				//navigate to course page
				coursePage2.courses();
			

	
				log.info("Enter the courseDetails page url");
				driver.get(prop.getProperty("autoAssessment"));
				
				//wait for the course insight overlay to disappear
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".section-content .clui-overlay-loading.light")));
				
				log.info("Successfully reached course detail page for autoAssessment course "); 
				
				SoftAssert softAssert = new SoftAssert();
				waitForAjax("Course details page");
				
				//softAssert.assertTrue(true, "enrol button is not visible ");
						
					
				//if form modal is already open
				boolean isModalPresent = driver.findElements(viewCoursePage.closeFormModal()).size()>0;
				
				//launch the form module
				//if form module is not open
				if(!isModalPresent)
				{
					log.info("not in form modal, so will click on launch button to open form modal");
					viewCoursePage.openModal(viewCoursePage.launchButton()); // open the form modal
				}
						
				//enter detail in form module
				viewCoursePage.formAutoAssessment();
					
				//click submit form
				viewCoursePage.submitModule();
					
				
				log.info("Form module Completed and Submitted");
						
				//send approval request
				viewCoursePage.sendForApproval();
				
				String successApproval= viewCoursePage.messageSubmitApproval();
					
				log.info("Success message for approval module submission - "+ successApproval );
				
				Assert.assertEquals(successApproval,"Your Approval request has been sent.", " It is failed as actual submit approval message is different from expected approval message. ");
				
				
				
				//validate the course
				validateCourseStatus(viewCoursePage.courseStatusElement());
				
						if(driver.findElements(viewCoursePage.reviewModal()).size()>0)
						//review the course
						{
							log.info("Review modal is enabled");
							viewCoursePage.review();
						}
						
						
						
						
				log.info("Enter the enrolment insight page url");
				driver.get(prop.getProperty("autoAssessmentEnrolment"));		
				waitForAjax("Enrolment insight page");		
				
				//wait for the course insight overlay to disappear
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".clui-overlay-loading")));
				
				
				
				//approve/reject the approval request
				enrolmentInsightPage.actionApproval("green");
				
				/*capturing approval success message*/
				String approvalNotificationMessage= enrolmentInsightPage.notificationMessage();
				
				
				//validate the approval notification message
				validateApprovalNotificationMessage(approvalNotificationMessage);
				
				/*-----------*/
				
				//open the assessment module
				enrolmentInsightPage.openAssessmentModal();
				
				
				//approve/reject the assessment
				enrolmentInsightPage.performAssessment("Passed");
				
				/*capturing assessment success message*/
				String assessmentNotificationMessage= enrolmentInsightPage.notificationMessage();
			
				//validate the approval notification message
				validateAssessmentNotificationMessage(assessmentNotificationMessage);
				
				/*-------------------*/
				
				enrolmentInsightPage.courseInsight();
				
				waitForAjax("Course insight page");
				
				log.info("Reached course insight page");
				
				String cellValue = ExcelUtil.getCellData(3, 1);
				
				// search for that user and reset the enrolment
					courseInsightPage2.searchUser(cellValue); 
					courseInsightPage2.editEnrolementStatus("Reset enrolment");
					
				/*capturing approval success message*/
				String resetNotificationMessage= enrolmentInsightPage.notificationMessage();	
				
				log.info("Enrolment reset was successfull");
				log.info("Success message- "+ resetNotificationMessage);
				
				//clear the search filter
				courseInsightPage2.clearFilter();
				log.info("User is removed from course insight");
				
				
				//logout method
				loginPage2.logoutQA();
				
				log.info("logged out");
				waitForAjax(" Home page after logout");
				
				
	}
	
	
	public void validateCourseStatus(By courseStatusElement)
	{
		log.info("Validating CourseStatus");
		String courseStatus= driver.findElement(courseStatusElement).getText().trim();
		log.info("The actual course status is - " + courseStatus);
		if(courseStatus.equalsIgnoreCase("In progress"))
		{
			log.info("The actual course status is valid");
			Assert.assertTrue(true);
		}
		else {
			log.info("The actual course status is not valid");
			Assert.assertTrue(false);
		}
		
	}
	
	public void validateApprovalNotificationMessage(String notificationMessage)
	{
		log.info("Success message for approval module submission - "+ notificationMessage );
		
		log.info("Validating approval notification message");
		
		if(notificationMessage.contains("has been set to Passed") || notificationMessage.contains("has been set to Competent"))
		{
			log.info("The approval notification message is valid");
			Assert.assertTrue(true);
		}
		else {
			log.info("The approval notification message is not valid");
			Assert.assertTrue(false);
		}
		
		log.info("Close the notification message");
		action.moveToElement(driver.findElement(By.cssSelector(".material-icons.btn-close.mdi-close"))).click().perform();
	}
		
	public void validateAssessmentNotificationMessage(String notificationMessage)
	{
		log.info("Success message for assessment module submission - "+ notificationMessage );
		
		log.info("Validating assessment notification message");
		
		Assert.assertEquals(notificationMessage,"Assessment for this enrolment has been saved.", " It is failed as actual submit approval message is different from expected approval message. ");
		
		log.info("Close the notification message");
		action.moveToElement(driver.findElement(By.cssSelector(".material-icons.btn-close.mdi-close"))).click().perform();
	}
	

}
