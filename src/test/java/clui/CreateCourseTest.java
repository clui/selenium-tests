package clui;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class CreateCourseTest extends BaseTest {
		
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        System.out.println("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("Sheet2");
	}
	
	
	@Test(priority=1)
	public void createNewCourse() throws InterruptedException, IOException
	{
		
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				EditCoursePage editCoursePage=new EditCoursePage(driver,wait, action, prop);
				
			
					
				//homepage methods
				driver.get(prop.getProperty("url"));
				int attemptnum =1;
			
				//login steps
				do{
					homePage.goToCredentials();
					
			
					//Loginpage method
					loginPage2.loginQA(ExcelUtil.getRowData(1)); //enter user name and password
					
			
					//wait for overlay to disappear
					loginPage2.loadingCourses();
					

					
						if(driver.findElements(profilePage2.myProfileLink()).size()>0) //check if user is logged in or not
						{
							break;
						}
				
				System.out.println(attemptnum + " login attempt failed for user -" + loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login" );
				attemptnum++; // add up the attempts
					
				}while((driver.findElements(By.id("clui-login-button")).size()>0));
					
				//login process end here
				
				//navigate to course page
				coursePage2.courses();
				
				//navigate to all courses tab
				coursePage2.allCourses();
				
				/* Stop this command for now and simply search the course
				 //call create course button click
				
				coursePage2.createCourse();
				
				
				*/
				
				String courseName=prop.getProperty("createCourseName"); //get the course name from data file
				
				//search for any course
				coursePage2.searchCourse(courseName);
				
				
				//navigate to edit course page
				coursePage2.editCourseCircle();
				
				
				/* Steps to create a course */
				editCoursePage.addSection("test1");
				
				
				
				
				
				
					
				
	}
	
	

}
