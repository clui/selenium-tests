package clui;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class CoursePerformTest extends BaseTest {
		
	public static Logger log= LogManager.getLogger(CoursePerformTest.class.getName());
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        log.info("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("login");
        
        log.info("**********************Testing - User is enrolling in a course, performing and then unenrolling from the same****************");
        log.info("Excel sheet- login is intialised");
	}
	
	
	@Test(priority=0)
	public void coursePerform() throws InterruptedException, IOException
	{
		
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				ViewCoursePage viewCoursePage=new ViewCoursePage(driver,wait, action, prop);
				CourseInsightPage2 courseInsightPage2=new CourseInsightPage2(driver,wait, action,prop);
			
					
				//homepage methods
				driver.get(prop.getProperty("url"));
				int attemptnum =1;
				boolean loginSuccess=false;
				waitForAjax("Home page");
				
				//login steps
				do{
					homePage.goToCredentials();
					// wait for page load
					waitForAjax("Login page");
			
					//Loginpage method
					//enter user name and password
					loginPage2.loginQA(ExcelUtil.getRowData(1)); 
					
					log.info("Credential entered");
					
					//wait for overlay to disappear
					loginPage2.loadingCourses();
					
					waitForAjax("After entering credentials");
					
					Thread.sleep(3000);

					// check if user is logged in or not
					if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) 
					{
						
						loginSuccess= true;
					}
					else if(driver.findElements(By.id("clui-login-button")).size() > 0)
					{
						loginSuccess= false;
						log.info(attemptnum + " login attempt failed for user -"
								+ loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login");
						attemptnum++; // add up the attempts
					}
					else 
					{
						log.info("The login credential has issues, please check");
						Assert.assertTrue(false);
					}
					

				} while (!loginSuccess);

				// login process end here
					
				
				//navigate to course page
				coursePage2.courses();
			

	
				log.info("Enter the courseDetails page url");
				driver.get(prop.getProperty("performCourseURL"));
				
				//wait for the course insight overlay to disappear
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".section-content .clui-overlay-loading.light")));
				
				log.info("Successfully reached course detail page"); 
				
				SoftAssert softAssert = new SoftAssert();
				waitForAjax("Course details page");
				
				/* After reaching the view course page, validate whether it has unenrol button or enrol button*/
				if(driver.findElements(viewCoursePage.enrolButtonElement()).size()>0)
				{
						softAssert.assertTrue(true, "enrol button is not visisble ");
						
						//call the validate method to validate the enrol button 
					validateEnrolButton(viewCoursePage.enrolButtonElement());
					
						//enroll in the course  
						viewCoursePage.enrollCourse();
						
						/*Now check the form modal is open or not*/
						if(driver.findElements(viewCoursePage.closeFormModal()).size()>0)
							{
								softAssert.assertTrue(true, "Form1 modal is not displayed ");
								log.info("form modal opened by default");
								viewCoursePage.formDetails();
							}
						else if(driver.findElements(viewCoursePage.launchButton()).size()>0)
							{
								softAssert.assertTrue(true, "launch button is not displayed ");
								log.info("not in form modal, so will click on launch button to open form modal");
								viewCoursePage.openModal(viewCoursePage.launchButton()); // open the form modal
								viewCoursePage.formDetails();
								
							}
						else
							{
								softAssert.assertTrue(false);
								log.info("some error");
							}
					
					
						//click submit form
						viewCoursePage.submitModule();
					
						log.info("Form module Completed and Submitted");
						
						
						//open quiz module
						viewCoursePage.quizModule("Get Started");
						
						//Quiz answers
						//Q1
						if(driver.findElements(By.xpath("//h2[contains(text(),'What is xxxx?')]")).size()>0)
						{
							log.info("Q1- What is xxxx? ");
							viewCoursePage.quizAnswers("Beer");
							log.info("Beer");
							Assert.assertTrue(true, "Answer is not correct");
							
							//Q2
							if(driver.findElements(By.xpath("//h2[contains(text(),'Superhero that smash everything?')]")).size()>0)
							{
								log.info("Q2- Superhero that smash everything? ");
								viewCoursePage.quizAnswers("Hulk");
								log.info("Hulk");
								Assert.assertTrue(true, "Answer is not correct");
								
								//Q3
								if(driver.findElements(By.xpath("//h2[contains(text(),'Best character in Batman trilogy?')]")).size()>0)
								{
									log.info("Q3- Best character in Batman trilogy? ");
									viewCoursePage.quizAnswers("Joker");									
									log.info("Joker");
									Assert.assertTrue(true, "Answer is not correct");
								}
							}
							
							
						}
						
						//Submit quiz module
						viewCoursePage.quizModule("Submit Quiz");
						
						
						if(driver.findElements(viewCoursePage.reviewModal()).size()>0)
						//review the course
						{
							log.info("Review modal is enabled");
							viewCoursePage.review();
						}
						
						//validate the course
						validateCourseStatus(viewCoursePage.courseStatusElement());
						
						
						
					}		
				
				//navigate to course insight page
				viewCoursePage.courseInsight();
				
				waitForAjax("Course insight page");
				
				log.info("Reached course insight page");
				
				String cellValue = ExcelUtil.getCellData(1, 1);
				
				// search for that user and unenrol from that course
					courseInsightPage2.searchUser(cellValue); 
					courseInsightPage2.removeUser();
			
				//clear the search filter
				courseInsightPage2.clearFilter();
				log.info("User is removed from course insight");
				
				
				//logout method
				loginPage2.logoutQA();
				
				log.info("logged out");
				waitForAjax(" Home page after logout");
				
				
	}
	
	
	public void validateCourseStatus(By courseStatusElement)
	{
		log.info("Validating CourseStatus");
		String courseStatus= driver.findElement(courseStatusElement).getText().trim();
		log.info("The actual course status is - " + courseStatus);
		if(courseStatus.equalsIgnoreCase("Passed")|| courseStatus.equalsIgnoreCase("Competent") )
		{
			log.info("The actual course status is valid");
			Assert.assertTrue(true);
		}
		else {
			log.info("The actual course status is not valid");
			Assert.assertTrue(false);
		}
		
	}
	
	public void validateEnrolButton(By enrolButtonElement )
	{
		
			log.info("Validating enrol button");
			//Check it is clickable and then click on it to enrol
			String enrolButtonClass= driver.findElement(enrolButtonElement).getAttribute("class");
			boolean isDisabled = enrolButtonClass.contains("disabled");
			
			if(isDisabled==true)
			{
				
				Assert.assertTrue(false, "The enrol button is disabled");

			}
			else
			{
				Assert.assertTrue(true);
				log.info("The enrol button is not disabled");
			}
			
		
		
		
	}
	

}
