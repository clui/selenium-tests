package clui;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.Listeners;
import clui.HomePage;

import utils.ExcelUtil;



@Listeners(CustomListener.class)
public class BrokenLinksTest extends BaseTest {
		
	public static Logger log= LogManager.getLogger(BrokenLinksTest.class.getName());
	
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        log.info("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("login");
        
  
        log.info("**********************Testing - Broken links of page****************");
        log.info("Excel sheet- login is intialised");
        
        
        
	}
	
	

	
	@Test(priority=0)
	public void brokenLinks() throws InterruptedException, IOException
	{
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				ErrorStatePage errorStatePage= new ErrorStatePage(driver,wait, action, prop);
				CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
				BasePage basePage=new BasePage(driver,wait, action, prop);
				
				
					log.info("Login with  " +  ExcelUtil.getCellData(1,0));
					
					
					
					//homepage methods
					driver.get(prop.getProperty("urlQA"));
					int attemptnum =1;
					boolean loginSuccess=false;
					waitForAjax("Home page");
					
					//login steps
						do{
							homePage.goToCredentials();
							// wait for page load
							waitForAjax("Login page");
					
							//Loginpage method
							//enter user name and password
							loginPage2.loginQA(ExcelUtil.getRowData(4)); 
							
							log.info("Credential entered");
					
							//wait for overlay to disappear
							loginPage2.loadingCourses();
					
							waitForAjax("After entering credentials");
							
							Thread.sleep(3000);
							
							// check if user is logged in or not
							if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) 
							{
								
								loginSuccess= true;
							}
							else if(driver.findElements(By.id("clui-login-button")).size() > 0)
							{
								loginSuccess= false;
								log.info(attemptnum + " login attempt failed for user -"
										+ loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login");
								attemptnum++; // add up the attempts
							}
							else 
							{
								log.info("The login credential has issues, please check");
								Assert.assertTrue(false);
							}
							

						} while (!loginSuccess);

						// login process end here
							
						//navigate to course page
						coursePage2.courses();
						
						//navigate to all courses tab
						coursePage2.allCourses();
						
						Thread.sleep(3000);

					
					/*check broken links*/
						basePage.brokenLink("https://qa.clui.com/");
						
				
						
					log.info("logging out");
					//logout method
					//loginPage2.logoutQA();
					
					//waitForAjax(" Home page after logout");
				}
	}
	

