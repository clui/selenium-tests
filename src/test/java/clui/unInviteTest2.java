package clui;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import clui.HomePage;

import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class unInviteTest2 extends BaseTest {
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        System.out.println("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("Sheet2");
	}
	
	@Test
	public void inviteUser() throws Exception 
	{
        
		//page instantiation
		HomePage homePage= new HomePage(driver, wait, action, prop);
		LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
		CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
		CourseInsightPage2 courseInsightPage2=new CourseInsightPage2(driver,wait, action,prop);
		ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
		
		
			
		
		//homepage methods
		driver.get(prop.getProperty("url"));
		int attemptnum =1;
	
		//login steps
		do{
			homePage.goToCredentials();
			
	
			//Loginpage method
			loginPage2.loginQA(ExcelUtil.getRowData(1)); //enter user name and password
			
	
			//wait for overlay to disappear
			loginPage2.loadingCourses();
	

			
				if(driver.findElements(profilePage2.myProfileLink()).size()>0) //check if user is logged in or not
				{
					break;
				}
		
		System.out.println(attemptnum + " login attempt failed for user -" + loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login" );
		attemptnum++; // add up the attempts
			
		}while((driver.findElements(By.id("clui-login-button")).size()>0));
			
		
			
			//navigate to course page
			coursePage2.courses();
			
			//navigate to all courses tab
			coursePage2.allCourses();
	
			String courseName=prop.getProperty("course"); //get the course name from data file
			
			//search for any course
			coursePage2.searchCourse(courseName);
			
			
			//click on insight option
			coursePage2.courseInsightCircle();
			
			
			//Set Test Data Excel and Sheet
			ExcelUtil.setExcelFileSheet("Sheet3");
			
			int i=1;
			String cellValue = ExcelUtil.getCellData(i, 1);
			
			while(cellValue != null)
			{
			
				courseInsightPage2.searchUser(cellValue); // search for that user 
				
				By emailOfUser= courseInsightPage2.emailOfSearchedUser(cellValue); // capture email address of searched user
				
				validateUser(cellValue, emailOfUser); // validate the searched user to match the cell value, so that it gets sure whether we are going to enroll the valid user or not
			
				courseInsightPage2.removeUser();
			
			i++;
			if(ExcelUtil.getCellData(i, 1)!= null) 
			{
				cellValue=ExcelUtil.getCellData(i, 1);
			}
			else 
			{
				cellValue=null;
			}
			
			
			//clear the search for next iteration
			courseInsightPage2.clearFilter();
			
			}
			
			
		
	
			
			//logout method
			loginPage2.logoutQA();
			

		
	}
	
	public void validateUser(String cellValue, By emailOfUser)
	{
		if(driver.findElement(emailOfUser).isDisplayed()==true)
		{
			
			//read text 
				if(cellValue.equals(driver.findElement(emailOfUser).getText())) 	

				{
					System.out.println("The searched user is correct and will be unenrolled");
					Assert.assertTrue(true);
				}
				else
				{
					System.out.println("The searched user is not the same as the cellvalue and will not be unenrolled");
					Assert.assertTrue(false);
				}
		}
		else 
		{
			System.out.println("The searched user is not valid");
			Assert.assertTrue(false);
		}
		
	}
	
	
	

		

			
		
}
