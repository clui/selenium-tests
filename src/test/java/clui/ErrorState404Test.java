package clui;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.Listeners;
import clui.HomePage;

import utils.ExcelUtil;
//import org.apache.logging.log4j.


@Listeners(CustomListener.class)
public class ErrorState404Test extends BaseTest {
		
	public static Logger log= LogManager.getLogger(ErrorState404Test.class.getName());
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
       log.info("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("404ErrorState");
        
        log.info("**********************Testing - Error state for 404 Page not found****************");
        log.info("Excel sheet- 404ErrorState is intialised");
	}
	
	

	
	@Test(priority=0)
	public void pageNotFound() throws InterruptedException, IOException
	{
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				ErrorStatePage errorStatePage= new ErrorStatePage(driver,wait, action, prop);
				CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
				 
				
				
					log.info("Login with  " + ExcelUtil.getCellData(1,1));
					
					
					
					//homepage methods
					driver.get(prop.getProperty("url"));
					int attemptnum =1;
					boolean loginSuccess=false;
					waitForAjax("Home page");
					
					//login steps
						do{
							homePage.goToCredentials();
							
							// wait for page load
							waitForAjax("Login page");
					
							//Loginpage method
							//enter user name and password
							loginPage2.loginQA(ExcelUtil.getRowData(1)); 
							
							log.info("Credential entered");
					
							//wait for overlay to disappear
							loginPage2.loadingCourses();
					
							waitForAjax("After entering credentials");
							
							Thread.sleep(3000);
							
							// check if user is logged in or not
							if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) 
							{
								
								loginSuccess= true;
							}
							else if(driver.findElements(By.id("clui-login-button")).size() > 0)
							{
								loginSuccess= false;
								log.info(attemptnum + " login attempt failed for user -"
										+ loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login");
								attemptnum++; // add up the attempts
							}
							else 
							{
								log.info("The login credential has issues, please check");
								Assert.assertTrue(false);
							}
							

						} while (!loginSuccess);

						// login process end here
							
						//navigate to course page
						coursePage2.courses();
						
						//navigate to all courses tab
						coursePage2.allCourses();

					
					/*initiate a loop to check all the error state*/
						
						int i=1;
						
						String errorStateUrl = ExcelUtil.getCellData(i, 3);
						
						while(errorStateUrl!= null)
						{
							//homepage methods
							driver.get(errorStateUrl);
							
							
							//wait for the page to completely load
							waitForAjax("404 not found state");
							String stateName= driver.findElement(errorStatePage.stateName()).getText().trim();
							Assert.assertEquals(stateName,ExcelUtil.getCellData(i, 4), "Error description - "+stateName+" should be " +ExcelUtil.getCellData(i, 4));
							log.info("404 page not found test is passed for state - " + stateName );
						
						
							String textCTA= driver.findElement(errorStatePage.buttonCTA()).getText().trim();
							Assert.assertEquals(textCTA,ExcelUtil.getCellData(i, 5), "Error description - "+textCTA+" should be " +ExcelUtil.getCellData(i, 5));
							log.info("The CTA button name is " + textCTA + ", as expected");
							
							if(textCTA.equalsIgnoreCase(ExcelUtil.getCellData(i, 5)))
							{
								errorStatePage.clickCTA();
								wait.until(ExpectedConditions.invisibilityOfElementLocated(errorStatePage.overlayHideCourse()));
							}
							else if(textCTA.equalsIgnoreCase(ExcelUtil.getCellData(i, 5)) || textCTA.equalsIgnoreCase(ExcelUtil.getCellData(i, 5)))
							{
								errorStatePage.clickCTA();
								wait.until(ExpectedConditions.invisibilityOfElementLocated(errorStatePage.overlayHideUsers()));
							}
						
							i++;
							//check to return only not null values
							if(ExcelUtil.getCellData(i, 3)!= null) 
							{
								errorStateUrl=ExcelUtil.getCellData(i, 3);
							}
							else 
							{
								errorStateUrl=null;
							}
						}
						
					log.info("logging out");
					//logout method
					loginPage2.logoutQA();
					
					waitForAjax(" Home page after logout");
				}
	}
	

