package clui;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import utils.ExcelUtil;
import org.openqa.selenium.Platform;

public class BaseTest {

	public WebDriver driver;
	public WebDriverWait wait;
	public Actions action;
	public Properties prop;
	public FileInputStream fis;
	
	public static Logger log= LogManager.getLogger(BaseTest.class.getName());

	
	//Global test data excel files
    public static final String testDataExcelFileName = "TestData2.xlsx";
    public static String currentTestName=new Throwable().getStackTrace()[0].getMethodName();
    
    public WebDriver getDriver() {
        return driver;
    }
    
    
    
    
	@BeforeClass(description = "Class Level Setup!")
	public void intializeBrowser(ITestContext context) throws IOException, InterruptedException
	{


		//set and initialize property file
		prop= new Properties();


		if(Platform.getCurrent().toString().equalsIgnoreCase("MAC"))
		{

			fis= new FileInputStream(System.getProperty("user.dir")+"//src//test//java//resources//Data.properties");
		}
		else if(Platform.getCurrent().toString().contains("WIN"))
		{
			fis= new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\java\\resources\\Data.properties");
		}

		
		prop.load(fis);
	
		String browserName;
		
		//just for error state IE driver to test
		/*if(this.getClass().getSimpleName().equalsIgnoreCase(" ") && LocalDate.now().getDayOfWeek().toString().equalsIgnoreCase("Tuesday"))
		{
			
			browserName= "IE";
			log.info("Setting browserName to - "+ browserName );
		}
		else */

			browserName=prop.getProperty("browser"); 
			log.info("The browser for testing- " + browserName); // print the browser name for test is running

			
			//if condition for chrome driver to use
			if(browserName.equals("chrome"))
			{
				if(Platform.getCurrent().toString().equalsIgnoreCase("MAC"))
				{System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//chromedriver_mac");}
				else if(Platform.getCurrent().toString().contains("WIN"))
				{System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\chromedriver.exe");}

			ChromeOptions options = new ChromeOptions();
	        options.addArguments("--ignore-certificate-errors"); 
	        options.addArguments("--start-maximized");
	        options.addArguments("--disable-extensions"); // disabling extensions
	        options.addArguments("--disable-gpu"); // applicable to windows os only
	        options.addArguments("--disable-dev-shm-usage"); // overcome limited resource problems
	        options.addArguments("--no-sandbox");
			
	        
	        log.info("Added chrome arguments");
	        
			//Create chrome driver
			driver = new ChromeDriver(options);
			
			}
			else if(browserName.equals("IE"))
			{
		        System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\IEDriverServer.exe");
		        
		        
				//Create IE driver
		        driver = new InternetExplorerDriver();
		        certIEPass();
			}
			
		
		
		context.setAttribute("webDriver", driver);
		
		//Maximize window
		driver.manage().window().maximize();
		
		//create wait
		wait= new WebDriverWait(driver,20);
		
		//create action
		action= new Actions(driver);
		
		

	
	}
	
	
	//handle certificate issue for IE
	public void certIEPass() throws InterruptedException
	{
		 driver.get("https://test2.qa.clui.com/courses");
		 
		 
		 driver.findElement(By.linkText("More information")).click();

		 driver.findElement(By.linkText("Go on to the webpage (not recommended)")).click();
		 

		 log.info("Sleep for 5 sec");
		 Thread.sleep(5000);
		 
	}
	
	 //create method to take screenshot
    public void takeScreenShot(String failedMethodName, WebDriver driver)
    {
    	
    	
    	TakesScreenshot ts=  (TakesScreenshot)driver;
    	
    	File source =ts.getScreenshotAs(OutputType.FILE);
    	
    	
    	try {
			FileUtils.copyFile(source, new File("./Screenshots/"+ failedMethodName + "_fail.png"));
		} catch (IOException e) {
		
			log.info("IOException is caught and handled- "+ e);
		}
    	
    	log.info("Screen captured");
    }
    
    
    public void waitForAjax(String currentMethod) throws InterruptedException{
        while (true)
        {
            Boolean ajaxIsComplete = (Boolean) ((JavascriptExecutor)driver).executeScript("return jQuery.active == 0");
        
          
            if (ajaxIsComplete ){
                break;
            }
            Thread.sleep(1000);
        }
        
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.id("page-loaded")));
        log.info("Page load and Ajax calls are completed for - " + currentMethod);
        
    }
    
  		
	
	@AfterClass
	public void closeWindow()
	{
		log.info("**********Quit the driver************************");
		driver.quit();
	}
	
	
	
}	
