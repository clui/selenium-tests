package clui;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.Listeners;
import clui.HomePage;

import utils.ExcelUtil;
//import org.apache.logging.log4j.


@Listeners(CustomListener.class)
public class LoginTests2 extends BaseTest {
		
	public static Logger log= LogManager.getLogger(LoginTests2.class.getName());
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        log.info("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("userlogins");
        
        log.info("**********************Testing - All types of login user test scenarios****************");
        log.info("Excel sheet - userlogins is intialised");
	}
	
	

	
	@Test(priority=2)
	public void validUserName() throws InterruptedException, IOException
	{
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				
				
				 
				for(int i=1; i<6; i++)
				{
					log.info("Test case type- " + loginPage2.testType(ExcelUtil.getRowData(i)));
					
					
					
					//homepage methods
					driver.get(prop.getProperty("url"));
					
					int attemptnum = 1;
					boolean loginSuccess=false;
					waitForAjax("Home page");

					
					//login steps
						do{
							homePage.goToCredentials();
							
							//wait for page load 
							waitForAjax("Login page");
					
							//Loginpage method
							//enter user name and password
							loginPage2.loginQA(ExcelUtil.getRowData(i)); 
							
							log.info("Credential entered");
					
							//wait for overlay to disappear
							loginPage2.loadingCourses();
					
							waitForAjax("After entering credentials");
							
							Thread.sleep(3000);
							
							// check if user is logged in or not
							if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) // check if user is logged in or not
							{
								
								loginSuccess= true;
							}
							else if(driver.findElements(By.id("clui-login-button")).size() > 0)
							{
								loginSuccess= false;
								log.info(attemptnum + " login attempt failed for user -"
										+ loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login");
								attemptnum++; // add up the attempts
							}
							else 
							{
								log.info("The login credential has issues, please check");
								Assert.assertTrue(false);
							}
							
						}while (!loginSuccess);
							
						// login process end here 

					
						//navigate to myprofile
						profilePage2.myProfile();
						log.info("reached on profile page");
						
						//wait for the page to completely load
						waitForAjax("profile page");
						
					//validate username for the user
					String info[] = profilePage2.profileInformation();
					
					log.info("Validate user profile information");
					if(info[0].equals(ExcelUtil.getRowData(i).getCell(3).toString()))
					{
						Assert.assertTrue(true);
					}
						else
					{
						Assert.assertTrue(false);
						log.info("The actual user name - "+ info[0] +", and the expected username - " + ExcelUtil.getRowData(i).getCell(3).toString());
					}
					
					
					log.info("logging out");
					//logout method
					loginPage2.logoutQA();
					
					waitForAjax(" Home page after logout");
					
				}
	}
	
	@Test(priority=1)
	public void invalid() throws IOException, InterruptedException
	{
       
				//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				
				
				log.info("Test case type- " + loginPage2.testType(ExcelUtil.getRowData(6)));
				
				
				//homepage methods
				driver.get(prop.getProperty("url"));
				
				//page load
				waitForAjax("Home page");
				
				homePage.goToCredentials();
				
				//wait for page load 
				waitForAjax("Login page");
				
				//create the loop to test all invalid case
				
				int invalidCaseCount= 1;
				String invalidCase = ExcelUtil.getCellData(invalidCaseCount, 0);
				
				while(invalidCase!= null)
				{
					if(invalidCase.contains("Invalid")) {
						//Loginpage method
						loginPage2.loginQA(ExcelUtil.getRowData(invalidCaseCount));
					
						log.info("entered credentials");
						
						//If it displays alert message than compare result with alert error type
						if(ExcelUtil.getCellData(invalidCaseCount, 5).equalsIgnoreCase("alert")) {
						//invalid user name method call
						String errorAlert= loginPage2.messageInvalidUser();
						
						log.info("validate error message for - "+ invalidCase );
						
						Assert.assertEquals(errorAlert,ExcelUtil.getCellData(invalidCaseCount, 4), " It is failed as actual and expected error message is different.-  ");
						log.info(invalidCase +" is passed and displayed the expected error message. ");
						}
						//If it displays frontend validation than compare result with front end error type
						else if(ExcelUtil.getCellData(invalidCaseCount, 5).equalsIgnoreCase("frontend validation")) {
							
							String validationError= loginPage2.frontendValidation();
							log.info("validate error message for - "+ invalidCase );
							
							Assert.assertEquals(validationError,ExcelUtil.getCellData(invalidCaseCount, 4), " It is failed as actual and expected error message is different.-  ");
							log.info(invalidCase +" is passed and displayed the expected error message. ");
						}
					}
				
					driver.navigate().refresh();
					
					invalidCaseCount++;
					//check to return only not null values
					if(ExcelUtil.getCellData(invalidCaseCount, 0)!= null) 
					{
						invalidCase=ExcelUtil.getCellData(invalidCaseCount, 0);
					}
					else 
					{
						invalidCase=null;
					}
				
				}
	}	
	
	
}
