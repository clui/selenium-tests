package clui;

import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class PaidCoursePerformTest extends BaseTest {

	public static Logger log = LogManager.getLogger(PaidCoursePerformTest.class.getName());

	@BeforeTest
	public void setupTestData() throws Exception {
		// Set Test Data Excel and Sheet
		log.info("************Setup Test Level Data**********");
		ExcelUtil.setExcelFileSheet("cardTest");

		log.info(
				"**********************Test - Buying course with different card details and checking the alert message in case of invalid card detail****************");
		log.info("Excel sheet- cardTest is intialised");
	}

	@Test(priority = 0)
	public void coursePerform() throws InterruptedException, IOException {

		// page instantiation
		HomePage homePage = new HomePage(driver, wait, action, prop);
		LoginPage2 loginPage2 = new LoginPage2(driver, wait, action, prop);
		CoursePage2 coursePage2 = new CoursePage2(driver, wait, action, prop);
		ProfilePage2 profilePage2 = new ProfilePage2(driver, wait, action, prop);
		ViewCoursePage viewCoursePage = new ViewCoursePage(driver, wait, action, prop);
		CourseInsightPage2 courseInsightPage2 = new CourseInsightPage2(driver, wait, action, prop);

		// homepage methods
		driver.get(prop.getProperty("url"));
		int attemptnum = 1;
		boolean loginSuccess = false;
		waitForAjax("Home page");

		// login steps
		do {
			homePage.goToCredentials();

			// wait for page load
			waitForAjax("Login page");

			// Loginpage method
			// enter user name and password
			loginPage2.loginQA(ExcelUtil.getRowData(1));

			log.info("Credential entered");

			// wait for overlay to disappear
			loginPage2.loadingCourses();

			waitForAjax("After entering credentials");

			Thread.sleep(3000);

			// check if user is logged in or not
			if (driver.findElements(profilePage2.myProfileLink()).size() > 0
					|| driver.findElements(loginPage2.userLogoutOption()).size() > 0) {

				loginSuccess = true;
			} else if (driver.findElements(By.id("clui-login-button")).size() > 0) {
				loginSuccess = false;
				log.info(attemptnum + " login attempt failed for user -" + loginPage2.testType(ExcelUtil.getRowData(1))
						+ ". So, will try again to login");
				attemptnum++; // add up the attempts
			} else {
				log.info("The login credential has issues, please check");
				Assert.assertTrue(false);
			}

		} while (!loginSuccess);

		// login process end here

		// navigate to course page
		coursePage2.courses();
		
		int i =1;
		String testType=ExcelUtil.getCellData(i, 0);
		
		
		
		// loop to test different type of card purchases
		while(testType != null) {
			log.info("Testing test scenario's for- " +testType );
			log.info("--------Enter the courseDetails page url------");
			driver.get(prop.getProperty("paidPerformCourseURL"));
			// wait for the course insight overlay to disappear
			wait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.cssSelector(".section-content .clui-overlay-loading.light")));

			log.info("------Successfully reached course detail page-------");

			SoftAssert softAssert = new SoftAssert();

			waitForAjax("Course details page");

			/*
			 * After reaching the view course page, validate whether it has unenrol button
			 * or enrol button
			 */
			if (driver.findElements(viewCoursePage.purchaseButtonElement()).size() > 0) {
				softAssert.assertTrue(true, "purchase button is not visisble ");
				log.info("inside purchase process ");

				// call the validate method to validate the enrol button
				validatePurchaseButton(viewCoursePage.purchaseButtonElement());

				// click on purchase button to enroll and purchase course
				viewCoursePage.clickPurchaseCourse();

				// validate the purchase method and accordingly purchase the course
				String cardType=validatePurchaseMethod(viewCoursePage.selectCardLabel());
				
				//loop for all invalid card details
				while(testType.equalsIgnoreCase("invalid purchase")) {
						//Intialise the card details
						String cardHolderName = ExcelUtil.getCellData(i, 3);
						String cardNumber = ExcelUtil.getCellData(i, 4);
						String expMonth = ExcelUtil.getCellData(i, 5);
						String expYear = ExcelUtil.getCellData(i, 6);
						String cvv = ExcelUtil.getCellData(i, 7);
						
						log.info("Card details entering for - " + testType );
						log.info("card holder name- " + cardHolderName + 
								"\n card number - " + cardNumber+ 
								"\n expiry month - " + expMonth + 
								"\n expiry year - " + expYear+
								"\n cvv - " + cvv);
						
						
						
						
						
						viewCoursePage.purchaseCard(cardType, cardHolderName, cardNumber, expMonth, expYear, cvv);
						
						//print message
						viewCoursePage.alertMessage();
						i++;
						
						testType=ExcelUtil.getCellData(i, 0);
	
						Thread.sleep(3000);
				}
				
				log.info("Scenario for valid purchase");
				
					//condition for valid card details
					if(testType.equalsIgnoreCase("valid purchase"))
					{
					
						//Intialise the card details
						String cardHolderName = ExcelUtil.getCellData(i, 3);
						String cardNumber = ExcelUtil.getCellData(i, 4);
						String expMonth = ExcelUtil.getCellData(i, 5);
						String expYear = ExcelUtil.getCellData(i, 6);
						String cvv = ExcelUtil.getCellData(i, 7);
						
						log.info("Card details entering for - " + testType + "with card type - " +  ExcelUtil.getCellData(i, 8) );
						log.info("card holder name- " + cardHolderName + 
								"\n card number - " + cardNumber+ 
								"\n expiry month - " + expMonth + 
								"\n expiry year - " + expYear+
								"\n cvv - " + cvv);
						
						
						viewCoursePage.purchaseCard(cardType, cardHolderName, cardNumber, expMonth, expYear, cvv);
						
						
					// wait for the purchase success message to appear
					wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".box-success")));

					// validate the purchase success message
					validatePurchaseSuccess(viewCoursePage.purchaseSuccessMessage());

					// close the purchase success modal
					viewCoursePage.viewCourseAction();

					/*-------------- Remove the user from the course ---------------*/

					// navigate to course insight page
					viewCoursePage.courseInsight();

					waitForAjax("Course insight page");

					log.info("Reached course insight page");

					String cellValue = ExcelUtil.getCellData(i, 1);

					// search for that user and unenrol from that course
					courseInsightPage2.searchUser(cellValue);
					courseInsightPage2.removeUser();

					// clear the search filter
					courseInsightPage2.clearFilter();
					log.info("User(" +cellValue+ ") that purchased the course is now removed from course insight page");
					i++;
				}
				
				
			}

			

			
			
			
			//check to return only not null values
			if(ExcelUtil.getCellData(i, 0)!= null) 
			{
				testType=ExcelUtil.getCellData(i, 0);
			}
			else 
			{
				testType=null;
			}
		}

		// logout method
		loginPage2.logoutQA();

		log.info("logged out");
		waitForAjax(" Home page after logout");
	}

	
	// method for validating the purchase success message
	public void validatePurchaseSuccess(By purchaseSuccessMessage) {
		log.info("Validating the purchase success  message");

		String purchaseSuccess = driver.findElement(purchaseSuccessMessage).getText().trim();
		if (purchaseSuccess.contains("Purchase successful!")) {
			log.info("The purchase success message is -" + purchaseSuccess);
			Assert.assertTrue(true);
			
		} else {
			log.info("The purchase success message is changed" + purchaseSuccess);
			Assert.assertTrue(false);
		}

	}

	public void validatePurchaseButton(By purchaseButtonElement) {

		log.info("------ validating purchase button-----");
		// Check it is clickable and then click on it to enrol
		String enrolButtonClass = driver.findElement(purchaseButtonElement).getAttribute("class");
		String enrolButtonPrice = driver.findElement(purchaseButtonElement).getText().trim();
		boolean isDisabled = enrolButtonClass.contains("disabled");

		if (isDisabled == true) {

			Assert.assertTrue(false, "The purchase button is disabled");

		} else {
			Assert.assertTrue(true);
			log.info("-------The purchase button is not disabled------");
			if (enrolButtonPrice.equalsIgnoreCase(prop.getProperty("paidCoursePrice"))) {
				log.info("------The price is as expected------");
				Assert.assertTrue(true, "The course price is different from expected");
			}

		}

	}

	public String validatePurchaseMethod(By selectCard) {
		log.info("-------- Validating card selection method------");

		// fetch the selection text and validate the card details for purchase
		String selectCardLabel = driver.findElement(selectCard).getText().trim();

		if (selectCardLabel.contains("Card ending in")) {
			Assert.assertTrue(true);
			log.info("It has a saved card detail");

		} else if (selectCardLabel.equalsIgnoreCase("Use a new card")) {
			Assert.assertTrue(true);
			log.info("It has no default save card, so will use new card details to purchase the course");
		} else {
			Assert.assertTrue(false, "The select card dropdown has some other option, please check");
		}

		return selectCardLabel;
	}

}
