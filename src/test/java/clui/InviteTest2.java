package clui;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import clui.HomePage;
import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class InviteTest2 extends BaseTest {
	
	public static Logger log= LogManager.getLogger(InviteTest2.class.getName());
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        log.info("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("login");
        
        log.info("**********************Testing - Invite users to a course and then unenrolling them from the same course****************");
        log.info("Excel sheet- login is intialised");
	}
	
	@Test
	public void inviteUser() throws Exception  
	{
        
		//page instantiation
		HomePage homePage= new HomePage(driver, wait, action, prop);
		LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
		CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
		ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
		CourseInsightPage2 courseInsightPage2=new CourseInsightPage2(driver,wait, action,prop);
		
		
		
			
		
			//homepage methods
			driver.get(prop.getProperty("url"));
			int attemptnum =1;
			boolean loginSuccess=false;
			waitForAjax("Home page");
			
			//login steps
			do{
				homePage.goToCredentials();
				
				// wait for page load
				waitForAjax("Login page");
				
				//Loginpage method
				//enter user name and password
				loginPage2.loginQA(ExcelUtil.getRowData(1)); 
				
				log.info("Credential entered");
				
				//wait for overlay to disappear
				loginPage2.loadingCourses();
		
				waitForAjax("After entering credentials");
				
				Thread.sleep(3000);
				
				// check if user is logged in or not
				if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) 
				{
					
					loginSuccess= true;
				}
				else if(driver.findElements(By.id("clui-login-button")).size() > 0)
				{
					loginSuccess= false;
					log.info(attemptnum + " login attempt failed for user -"
							+ loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login");
					attemptnum++; // add up the attempts
				}
				else 
				{
					log.info("The login credential has issues, please check");
					Assert.assertTrue(false);
				}
					
			}while (!loginSuccess);
				
			// login process end here
			
			
			//navigate to course page
			coursePage2.courses();
			
			log.info("reached on course page");
			
			//wait for the page to completely load
			waitForAjax("Course page");
			
			//navigate to all courses tab
			coursePage2.allCourses();
	
			String courseName=prop.getProperty("course"); //get the course name from data file
			
			//search for any course
			coursePage2.searchCourse(courseName);
			
			log.info("search for course");
		
			
			//click on bubble menu
			coursePage2.bubbleMenu();
			
			//click on assign course option
			coursePage2.clickAssignCourseOption();
			
			log.info("Start assigning course to learners");
			
			
	
			//Set Test Data Excel and Sheet
			ExcelUtil.setExcelFileSheet("userInviteList");
			
			int i=1;
			
			String cellValue = ExcelUtil.getCellData(i, 1);
			
			//keep this loop until the user list in empty
			while(cellValue != null)
			{
			
				coursePage2.addUser(cellValue); // add user to the invite list
				i++;
				
				//check to return only not null values
				if(ExcelUtil.getCellData(i, 1)!= null) 
				{
					cellValue=ExcelUtil.getCellData(i, 1);
				}
				else 
				{
					cellValue=null;
				}
				
			
			}
			log.info("All users are added to send invite list");
			
			//remove the email link
			coursePage2.untickEmailLink();
			
			//the number of invited users
			int numOfInvitedUsers = i-1;
			
			//put the alert success message under try catch block 
			try {
				// call the send invite method on assign modal
				coursePage2.sendInvite();
				
				waitForAjax(" invite");
				
				//Store invite success message in string variable
				String successInviteMessage= coursePage2.successAlert();
				
				log.info("The invite success message is-  " + successInviteMessage);
				Assert.assertEquals(successInviteMessage,"You've assigned 1 course", " Failed as actual and expected error message is different for invite user  ");
				
			}catch(TimeoutException toe)
			{
				log.info("Exception caught and handled- " + toe);
				log.info("Could not capture invite success message because of timeout exception");
			}
			
			log.info("-------Now unenrol those invited users-------");
			
			//click on insight option
			coursePage2.courseInsightCircle();
			
			waitForAjax("Course Insight");
			
			log.info("reached course insight page");

			
			int k=1;
			cellValue = ExcelUtil.getCellData(k, 1);
			log.info("Search each user from invite list and unenrol");
			while(cellValue != null)
			{
				// search for that user 
				courseInsightPage2.searchUser(cellValue);
				
				// capture email address of searched user
				By emailOfUser= courseInsightPage2.emailOfSearchedUser(cellValue); 
				
				// validate the searched user to match the cell value, so that it gets sure whether we are going to enroll the valid user or not
				validateUser(cellValue, emailOfUser); 
			
				courseInsightPage2.removeUser();
			
			k++;
			if(ExcelUtil.getCellData(k, 1)!= null) 
			{
				cellValue=ExcelUtil.getCellData(k, 1);
			}
			else 
			{
				cellValue=null;
			}
			
			
			//clear the search for next iteration
			courseInsightPage2.clearFilter();
			
			
			}
			
			log.info("Logging out");
			
			
			//logout method
			loginPage2.logoutQA();
			
			waitForAjax(" Home page after logout");
		
	}
	
		
		public void validateUser(String cellValue, By emailOfUser)
		{
			if(driver.findElement(emailOfUser).isDisplayed()==true)
			{
				
				//read text 
					if(cellValue.equals(driver.findElement(emailOfUser).getText())) 	

					{
						log.info("The searched user is correct and will be unenrolled");
						Assert.assertTrue(true);
					}
					else
					{
						log.info("The searched user is not the same as the cellvalue and will not be unenrolled");
						Assert.assertTrue(false);
					}
			}
			else 
			{
				log.info("The searched user is not valid");
				Assert.assertTrue(false);
			}
			
		}
		
}
