package clui;

import static org.testng.Assert.assertTrue;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import utils.ExcelUtil;

@Listeners(CustomListener.class)
public class CoursePerformPathBasedTest extends BaseTest {
		
	public static Logger log= LogManager.getLogger(CoursePerformPathBasedTest.class.getName());
	
	boolean resetTest;
	
	@BeforeTest
	public void setupTestData() throws Exception
	{
		//Set Test Data Excel and Sheet
        log.info("************Setup Test Level Data**********");
        ExcelUtil.setExcelFileSheet("login");
        
        log.info("**************Testing path based course for different actions****************");
        log.info("Excel sheet- login is intialised");
        
	}
	
	@Test(priority=0)
	public void coursePerform() throws InterruptedException, IOException
	{
		
		//page instantiation
				HomePage homePage= new HomePage(driver, wait, action, prop);
				LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
				CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
				ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
				ViewCoursePage viewCoursePage=new ViewCoursePage(driver,wait, action, prop);
				CourseInsightPage2 courseInsightPage2=new CourseInsightPage2(driver,wait, action,prop);
			
					
				String[] pathName = {"QLD", "VIC", "NSW", "ACT", "NT" ,"TAS"};
				
				for(int pathNum=0; pathNum<6; pathNum++ )
				{
					log.info("-------------Testing for path section - " + pathName[pathNum] );
					
					 //homepage methods
					driver.get(prop.getProperty("url"));
					int attemptnum =1;
					boolean loginSuccess=false;
					waitForAjax("Home page");
					
					//make the user to run the test case 5 times to cover all path options
					
					
					//login steps
					do{
						homePage.goToCredentials();
						// wait for page load
						waitForAjax("Login page");
				
						//Loginpage method
						//enter user name and password
						loginPage2.loginQA(ExcelUtil.getRowData(1)); 
						
						log.info("Credential entered");
						
						//wait for overlay to disappear
						loginPage2.loadingCourses();
						
						waitForAjax("After entering credentials");
						
						Thread.sleep(3000);
				
						// check if user is logged in or not
						if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) 
						{
							
							loginSuccess= true;
						}
						else if(driver.findElements(By.id("clui-login-button")).size() > 0)
						{
							loginSuccess= false;
							log.info(attemptnum + " login attempt failed for user -"
									+ loginPage2.testType(ExcelUtil.getRowData(1)) + ". So, will try again to login");
							attemptnum++; // add up the attempts
						}
						else 
						{
							log.info("The login credential has issues, please check");
							Assert.assertTrue(false);
						}
						
				
					} while (!loginSuccess);
				
					// login process end here
				
				//navigate to course page
				coursePage2.courses();
			

	
				log.info("Enter the courseDetails page url");
				driver.get(prop.getProperty("autoPath"));
				
				//wait for the course insight overlay to disappear
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".section-content .clui-overlay-loading.light")));
				
				log.info("Successfully reached course detail page"); 
				
				SoftAssert softAssert = new SoftAssert();
				waitForAjax("Course details page");
				
				/* After reaching the view course page, validate whether it has unenrol button or enrol button*/
			if(driver.findElements(viewCoursePage.enrolButtonElement()).size()>0)
				{
						softAssert.assertTrue(true, "enrol button is not visisble ");
						
					
						//enroll in the course  
						viewCoursePage.enrollCourse();
					
						boolean isModalPresent = driver.findElements(viewCoursePage.closeFormModal()).size()>0;
						
						//launch the path module
						//if path module is not open
						if(!isModalPresent)
						{
							log.info("not in form modal, so will click on launch button to open form modal");
							viewCoursePage.openModal(viewCoursePage.launchButton()); // open the form modal
						}
							
						
						
						
						log.info("Select the path on path module");
						
						
						//click submit form
						viewCoursePage.pathModule(pathName[pathNum]);
					
						log.info("Path selected for - " +pathName[pathNum]);
						
						
						//validate the eabled section after the path selection
						validatePathSection(viewCoursePage.pathSectionElement(), pathName[pathNum]);
					
						
						//open the form modal of selected path 
						viewCoursePage.openModal(By.cssSelector(".list-section li.section:nth-child(2) .lbl-button"));
						
						
						//enter the details in state form
						viewCoursePage.stateFormDetails();
						
						//click submit form
						viewCoursePage.submitModule();
						
						
						log.info("Form module of " + pathName[pathNum] + " is completed and submitted"); 
						
						//if review modal appears
						if(driver.findElements(viewCoursePage.reviewModal()).size()>0)
							//review the course
							{
								log.info("Review modal is enabled");
								viewCoursePage.review();
							}
						
						
						Thread.sleep(5000);
						
						//validate the course on the basis of action module
						validateCourseStatus(viewCoursePage.courseStatusElement(), pathName[pathNum] );
						
						
					}		
				
				//navigate to course insight page
				viewCoursePage.courseInsight();
				
				waitForAjax("Course insight page");
				
				log.info("Reached course insight page");
				
				String cellValue = ExcelUtil.getCellData(1, 1);
				
				// search for that user and unenrol from that course
					courseInsightPage2.searchUser(cellValue); 
					courseInsightPage2.removeUser();
			
				//clear the search filter
				courseInsightPage2.clearFilter();
				log.info("User is removed from course insight");
				
				
				//logout method
				loginPage2.logoutQA();
				
				log.info("logged out");
				waitForAjax(" Home page after logout");
				
				
				
				}
				
	}
	
	@Test(priority=1)
	public void resetCourse() throws InterruptedException, IOException
	{
		  LocalDate localDate = LocalDate.now();
			
		  //this will make this test case run on monday and friday only. Although require to find a better way to run this script
		String dayOfWeek= localDate.getDayOfWeek().toString();
		int dayOfWeekNum=  localDate.getDayOfWeek().getValue();
		if(dayOfWeek.equalsIgnoreCase("MONDAY") || dayOfWeek.equalsIgnoreCase("FRIDAY"))
		{	
		
		//page instantiation
		HomePage homePage= new HomePage(driver, wait, action, prop);
		LoginPage2 loginPage2=new LoginPage2(driver,wait, action, prop);
		CoursePage2 coursePage2=new CoursePage2(driver,wait, action, prop);
		ProfilePage2 profilePage2=new ProfilePage2(driver,wait, action, prop);
		ViewCoursePage viewCoursePage=new ViewCoursePage(driver,wait, action, prop);
		CourseInsightPage2 courseInsightPage2=new CourseInsightPage2(driver,wait, action,prop);
	
		//homepage methods
		driver.get(prop.getProperty("url"));
		int attemptnum =1;
		boolean loginSuccess=false;
		waitForAjax("Home page");
		
		//make the user to run the test case 5 times to cover all path options
		
		
		//login steps
		do{
			homePage.goToCredentials();
			// wait for page load
			waitForAjax("Login page");
	
			//Loginpage method
			//enter user name and password
			loginPage2.loginQA(ExcelUtil.getRowData(2)); 
			
			log.info("Credential entered");
			
			//wait for overlay to disappear
			loginPage2.loadingCourses();
			
			waitForAjax("After entering credentials");
			
			Thread.sleep(3000);

			// check if user is logged in or not
			if (driver.findElements(profilePage2.myProfileLink()).size() > 0 || driver.findElements(loginPage2.userLogoutOption()).size() > 0) 
			{
				
				loginSuccess= true;
			}
			else if(driver.findElements(By.id("clui-login-button")).size() > 0)
			{
				loginSuccess= false;
				log.info(attemptnum + " login attempt failed for user -"
						+ loginPage2.testType(ExcelUtil.getRowData(2)) + ". So, will try again to login");
				attemptnum++; // add up the attempts
			}
			else 
			{
				log.info("The login credential has issues, please check");
				Assert.assertTrue(false);
			}
			

		} while (!loginSuccess);

		// login process end here--------------------------------
			
		
		//navigate to course page
		coursePage2.courses();
	


		log.info("Enter the courseDetails page url");
		driver.get(prop.getProperty("autoPath"));
		
		//wait for the course insight overlay to disappear
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".section-content .clui-overlay-loading.light")));
		
		log.info("Successfully reached course detail page"); 
		
		SoftAssert softAssert = new SoftAssert();
		waitForAjax("Course details page");
		
		
		
		//condition to validate the reset date
		if(dayOfWeek.equalsIgnoreCase("MONDAY") )
		{
			//this block will validate the course status and completes the course again to see the reset message
			validateCourseStatus(viewCoursePage.courseStatusElement(), "RESET" );
			
			//complete the course-------------------------------------------------------------------------------
			boolean isModalPresent = driver.findElements(viewCoursePage.closeFormModal()).size()>0;
			//if path module is not open
			if(!isModalPresent)
			{
				log.info("not in form modal, so will click on launch button to open form modal");
				viewCoursePage.openModal(viewCoursePage.launchButton()); // open the form modal
			}
				
			
			
			
			log.info("Select the path on path module");
			
			
			//click submit form
			viewCoursePage.pathModule("TAS");
		
			log.info("Path selected for - " + "TAS");

			//open the form modal of selected path 
			viewCoursePage.openModal(By.cssSelector(".list-section li.section:nth-child(2) .lbl-button"));
			
			
			//enter the details in state form
			viewCoursePage.stateFormDetails();
			
			//click submit form
			viewCoursePage.submitModule();
			
			
			log.info("Form module of " + "TAS" + " is completed and submitted"); 
			
			//if review modal appears
			if(driver.findElements(viewCoursePage.reviewModal()).size()>0)
				//review the course
				{
					log.info("Review modal is enabled");
					viewCoursePage.review();
				}
			
			
			Thread.sleep(5000);
			
			//validate the course on the basis of action module
			validateCourseStatus(viewCoursePage.courseStatusElement(), "TAS" );
			
			//validate the number for reset date
			validateResetNumber(dayOfWeekNum);
			
		}
		else if(dayOfWeek.equalsIgnoreCase("FRIDAY") )
		{
			//validate the number for reset date
			validateResetNumber(dayOfWeekNum);
			
			//if review modal appears
			if(driver.findElements(viewCoursePage.reviewModal()).size()>0)
				//review the course
				{
					log.info("Review modal is enabled");
					viewCoursePage.review();
				}
			
			
		}
		
		//logout the user
		//logout method
		loginPage2.logoutQA();
		
		log.info("logged out");
		waitForAjax(" Home page after logout");
		
		
		}
		
		//not run the rest of the days
		else {
			log.info("This test is only made to run on Monday and Friday for particular setting");
		}
		
	}
	
	//returns the number of days left to reset the course
	public void validateResetNumber(int dayOfWeekNum)
	{
				int expectedDaysLeftToReset=7-dayOfWeekNum;
				SoftAssert softAssert = new SoftAssert();
				
				String resetMessage= driver.findElement(By.cssSelector(".box-message-body")).getText().trim();
				log.info("validating reset message for the numbers of days left for the course to reset - "+ resetMessage);
				
				if(resetMessage.contains(expectedDaysLeftToReset+ " days"))
				{
					log.info("The days left for the course to reset is "+ expectedDaysLeftToReset);
					softAssert.assertTrue(true);
				}
				else {
					log.info("The days left are invalid as per the test scenario, please review");
					softAssert.assertTrue(false, "please check");
				}
				
	}
	
	public void validateCourseStatus(By courseStatusElement, String pathName)
	{
		log.info("Validating CourseStatus on the basis of action module");
		String courseStatus= driver.findElement(courseStatusElement).getText().trim();
		log.info("The actual course status is - " + courseStatus);
		
		SoftAssert softAssert = new SoftAssert();
		
		String path = pathName;
		
		switch (path)
		{
		case "QLD":
			if(courseStatus.equalsIgnoreCase("Passed")|| courseStatus.equalsIgnoreCase("Competent") )
			{
				log.info("The actual course status is valid");
				softAssert.assertTrue(true);
			}
			else {
				softAssert.assertTrue(false, "The course has some other status, please check");
			}
			break;
		case "NSW":
			if(courseStatus.equalsIgnoreCase("Failed")|| courseStatus.equalsIgnoreCase("Not yet passed") || courseStatus.equalsIgnoreCase("Not yet competent") )
			{
				log.info("The actual course status is valid");
				softAssert.assertTrue(true);
			}
			else {
				softAssert.assertTrue(false, "The course has some other status, please check");
			}
			break;
		case "VIC":
			if(courseStatus.equalsIgnoreCase("Passed")|| courseStatus.equalsIgnoreCase("Competent"))
			{
				log.info("The actual course status is valid");
				softAssert.assertTrue(true);
			}
			else {
				softAssert.assertTrue(false, "The course has some other status, please check");
			}
			break;
		case "ACT":
			if(courseStatus.equalsIgnoreCase("In progress"))
			{
				log.info("The actual course status is valid");
				softAssert.assertTrue(true);
			}
			else {
				softAssert.assertTrue(false, "The course has some other status, please check");
			}
			break;
		case "NT":
			if(courseStatus.equalsIgnoreCase("Passed")|| courseStatus.equalsIgnoreCase("Competent") )
			{
				log.info("The actual course status is valid");
				softAssert.assertTrue(true);
			}
			else {
				softAssert.assertTrue(false, "The course has some other status, please check");
			}
			break;
		case "TAS":
			//only this path will have the reset course message
			
			String resetMessage= driver.findElement(By.cssSelector(".box-message-body")).getText().trim();
			log.info("The reset message - "+ resetMessage);
			
			if(courseStatus.equalsIgnoreCase("Passed")|| courseStatus.equalsIgnoreCase("Competent")|| resetMessage.contains("6 days"))
			{
				log.info("The actual course status is valid");
				softAssert.assertTrue(true);
			}
			else {
				softAssert.assertTrue(false, "The course has some other status, please check");
			}
			break;
		case "RESET":
			if(courseStatus.equalsIgnoreCase(" Yet to start"))
			{
				log.info("The actual course status is valid");
				softAssert.assertTrue(true);
			}
			else {
				softAssert.assertTrue(false, "The course has some other status, please check");
			}
			break;
		default: 
            log.info("no match"); 
		}
			
	}
	

	public void validatePathSection(By pathSectionElement, String pathName)
	{
		log.info("Validating the enabled section ");
		String sectionName= driver.findElement(pathSectionElement).getText().trim();
		log.info("The section enabled is - " + sectionName);
		if(sectionName.equalsIgnoreCase(pathName))
		{
			log.info("The expected path is enabled");
			Assert.assertTrue(true, "Some different path section is got enabled, please check");
		}
		
	}

}
