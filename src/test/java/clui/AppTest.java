package clui;

import static org.junit.Assert.assertTrue;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.asserts.SoftAssert;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
    	int daysLeftToReset=7;
		//logic for calculating days left to reset the course
		for(int dayOfWeekNum=1; dayOfWeekNum<=7; dayOfWeekNum++)
		{
			daysLeftToReset= 7-dayOfWeekNum;
			System.out.println("for day of week("+dayOfWeekNum+")"+ "the days left is - "+ daysLeftToReset);
		}
    }
       
}
