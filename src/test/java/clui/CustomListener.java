package clui;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class CustomListener extends BaseTest implements ITestListener{

	
	

	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailure(ITestResult result) {
		

		log.info(" <<<<<<<<<<<<<<<<<<<<<<Error- " + result.getName() + " test has failed ");
		
		String failedMethodName= result.getName().toString().trim();
		
		WebDriver driver=(WebDriver)result.getTestContext().getAttribute("webDriver");
		
		takeScreenShot(failedMethodName, driver);
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

}
