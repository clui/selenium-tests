package utils;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Platform;


public class ExcelUtil {
	


	
				//Global test data excel files
				public static final String testDataExcelFileName = "TestData2.xlsx";
    
				//main directory of project
				public static final String currentDir= System.getProperty("user.dir");
		
			
				//location of test data excel file path 
				public static String testDataExcelPath= null;
			
				
				//Excel workbook and excel sheet
				private static XSSFWorkbook excelWBook;
				private static XSSFSheet excelWSheet;
			
			
				//Excel cell and row
				private static XSSFCell cell;
				private static XSSFRow row;
			
			
				//row number  and column number
				private static int rowNumber;
				private static int columnNumber;
			

				//setter and getter for rows and columns
				public static void setRowNumber(int prowNumber)
				{
					rowNumber= prowNumber;
				}
			
				public static int getRowNumber()
				{
					return rowNumber;
				}
			
				public static void setColumnNumber(int pcolumnNumber)
				{
					columnNumber= pcolumnNumber;
				}
			
				public static int getColumnNumber()
				{
					return columnNumber;
				}
				
			
				//This method has two parameters: test data excel file name and excel sheet name 
				//check exception in the end
				public static void setExcelFileSheet(String sheetname) throws Exception 
				{
					//verify for mac or window excel path
					if(Platform.getCurrent().toString().equalsIgnoreCase("MAC"))
					{
						testDataExcelPath= currentDir + "//src//test//java//resources//";
					}
					else if(Platform.getCurrent().toString().contains("WIN"))
					{
						testDataExcelPath= currentDir + "\\src\\test\\java\\resources\\";
					}
			
					

					//open the excel file login user

					try {
						FileInputStream ExcelFile= new FileInputStream(testDataExcelPath + testDataExcelFileName);
						excelWBook= new XSSFWorkbook(ExcelFile);
						excelWSheet = excelWBook.getSheet(sheetname);
						} catch (Exception e)
						{	
							try {
				                throw (e);
				            } catch (IOException e1) {
				                e1.printStackTrace();
				            }
						}
					
				
				}
				
				
				
				//method to read cell data from excel file
				public static String getCellData(int rowNum, int colNum) 
				{
						if(excelWSheet.getRow(rowNum)!=null)
						{
							cell= excelWSheet.getRow(rowNum).getCell(colNum);
							DataFormatter formatter= new DataFormatter();
							String cellData= formatter.formatCellValue(cell);
							
								return cellData.trim();
							
						}
						
						else
						{
							return null;
						}
						
				}
				
				//method to return row number
				public static XSSFRow getRowData(int rowNum)
				{
					row= excelWSheet.getRow(rowNum);
					return row;
					
				}
				
				
			    //This method gets excel file, row and column number and set a value to the that cell.
			    public static void setCellData(String value, int rowNum, int colNum) throws Exception {
			        try {
			            row = excelWSheet.getRow(rowNum);
			            cell = row.getCell(colNum);
			            if (cell == null) {
			                cell = row.createCell(colNum);
			                cell.setCellValue(value);
			            } else {
			                cell.setCellValue(value);
			            }
			            // Constant variables Test Data path and Test Data file name
			            FileOutputStream fileOut = new FileOutputStream(testDataExcelPath + testDataExcelFileName);
			            excelWBook.write(fileOut);
			            fileOut.flush();
			            fileOut.close();
			        } catch (Exception e) {
			            try {
			                throw (e);
			            } catch (IOException e1) {
			                e1.printStackTrace();
			            }
			        }
			    }
}
