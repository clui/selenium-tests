package clui;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.ExcelUtil;

import org.openqa.selenium.interactions.Actions;


public class EnrolmentInsightPage extends BasePage{

	public static Logger log= LogManager.getLogger(EnrolmentInsightPage.class.getName());
	
	public EnrolmentInsightPage(WebDriver driver, WebDriverWait wait, Actions action, Properties prop) 
	{
		super(driver, wait, action,prop);
		
	}


	//return assessment action menu drop down css
	public By assessmentActionMenu() 
	{					
		return By.cssSelector(".bubble-info.ui-info-bubble-info.dropdown.hover .menu-actions li");
	}
	
	//approve the approval request
		public void actionApproval(String action) throws InterruptedException
		{
			Thread.sleep(3000);
			
			//click on action button on approval module
			moveToElementClick(By.cssSelector(".box-action a[ng-class=\"{'md-hide':!(moduleEnrolment.type == 'UploadFileModule' || moduleEnrolment.type=='ApprovalModule')}\"] "));
			
			//wait for the course insight overlay to disappear
			invisibiltyOfElement(By.cssSelector(".clui-overlay-loading"));
			
			Thread.sleep(3000);
			
			log.info("Action to do on approval module- "+ action);
			
			//approve or reject the request
			moveToElementClick(By.cssSelector("."+ action +".ui-button"));
			
			
			//wait for the course insight overlay to disappear
			invisibiltyOfElement(By.cssSelector(".clui-overlay-loading"));
			
		}
		
		//approval success message of approving/rejecting approval request
		public String notificationMessage()
		{
			visibilityOfElement(By.cssSelector("div[class='box-message']"));
			String successMessage= readText(By.cssSelector("div[class='box-message']"));
					
			return successMessage;
					
		}
		
		//navigate to course insight page
		public void courseInsight()
		{
			moveToElementClick(By.id("button-course-insight"));
			//wait for the course insight overlay to disappear
			invisibiltyOfElement(By.cssSelector(".section-content .clui-overlay-loading.alpha"));
			
		}
		
		//open the assessment module
		public void openAssessmentModal() throws InterruptedException 
		{
			log.info("Click on assessment Action button  ");
			
			//click on action button on assessment module
			moveToElementClick(By.cssSelector(".box-action div[ng-if=\"hasPermissionOnModuleAction(moduleEnrolment.type) && moduleEnrolment.type=='AssessmentModule' && !couponid && moduleEnrolment.accessible\"] "));
			
			
			//wait for perform assessment option to appear
			visibilityOfElement(By.cssSelector(".bubble-info.ui-info-bubble-info.dropdown.hover"));
			
			Thread.sleep(3000);
			
			log.info("Click on perform assessment option from drop down ");
			//click perform assessment option
			selectListOption(assessmentActionMenu(), "Perform assessment");
			
			//wait for assessment modal to appear completely
			visibilityOfElement(By.cssSelector(".box-content .button-close-modal"));
			Thread.sleep(3000);
					
		}
		
		//perform the assessment 
		public void performAssessment(String action) throws InterruptedException 
		{
			log.info("Reached on assessment modal  ");
					
			//write sample text on assessment module
			writeText(By.cssSelector(".ui-input-textarea"), "This is test field for writing age");
			Thread.sleep(3000);
					
			log.info("Action to do on assessment modal- "+ action);
					
			//approve or reject the request
			moveToElementClick(By.cssSelector("div[ng-click=\"form.result='PASSED'\"] "));
					
			Thread.sleep(3000);
			
			log.info("assessment approval is done ");
		}
		
	
}
