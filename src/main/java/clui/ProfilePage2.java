package clui;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class ProfilePage2 extends BasePage {
	
	public static Logger log = LogManager.getLogger(ProfilePage2.class.getName());

	public ProfilePage2(WebDriver driver, WebDriverWait wait, Actions action, Properties prop)
	{
		super(driver, wait, action, prop);
		
	}
	
	//web elements
	
	public By myProfileLink()
	{
		return By.cssSelector("header[id='header'] a[href='/profile']");
		
	}
	
	//navigate to my profile page
	public void myProfile() throws InterruptedException
	{
		//wait for the element to appear
		visibilityOfElement(myProfileLink());
		
		//click on myprofile link
		moveToElementClick(myProfileLink());
		
		//wait a while to load the page
		Thread.sleep(2000);
		
		//click on profile tab
		moveToElementClick(By.id("tab-profile"));
	}
	
	
	//return profile information
	public String[] profileInformation( )
	{
		String info[] = new String[3];
		info[0]= readText(By.id("box-user-name")); //username of that user
		info[1]= readText(By.id("profile-info-accessLevels")); // access level of that user
		info[2]= readText(By.id("profile-info-email")); //email of that user
		
		log.info("The username of user logged in is :" + info[0]);
		log.info("The access level of this user is :" + info[1] );
		log.info("The email address of user logged in is :" + info[2]);
		
		return info;
			
	}
	
}
