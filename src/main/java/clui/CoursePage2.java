package clui;

import java.awt.List;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.ExcelUtil;

import org.openqa.selenium.interactions.Actions;


public class CoursePage2 extends BasePage{

	public static Logger log= LogManager.getLogger(CoursePage2.class.getName());
	
	public CoursePage2(WebDriver driver, WebDriverWait wait, Actions action, Properties prop) 
	{
		super(driver, wait, action, prop);
		
	}

	//Global test data excel files
    public static final String testDataExcelFileName = "TestData2.xlsx";
    
	//web elements
	
	//navigate to courses page
	public void courses()
	{
		
		//wait for course link to be click able 
		visibilityOfElement(By.cssSelector("header[id='header'] a[href='/courses']"));
		elementToBeClickable(By.cssSelector("header[id='header'] a[href='/courses']"));
		
		moveToElementClick(By.cssSelector("header[id='header'] a[href='/courses']"));
		
		
	}
	
	//navigate to all courses tab
	public void allCourses()
	{
		//wait for all course tab link to be click able 
				visibilityOfElement(By.cssSelector(".widget-courses-new a[href='#allCourses']"));
				elementToBeClickable(By.cssSelector(".widget-courses-new a[href='#allCourses']"));
				
				moveToElementClick(By.cssSelector(".widget-courses-new a[href='#allCourses']"));
				
				//wait for overlay to disappear
				invisibiltyOfElement(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses"));
				
	}
	
	
	//search any course 
	public void searchCourse(String courseName)
	{
		//wait for search field to appear
		visibilityOfElement(By.cssSelector("div[data-mode='allCourses'] input[type='text']"));
		
		
		//enter text in search field
		writeText(By.cssSelector("div[data-mode='allCourses'] input[type='text']"), courseName );
		
		//click on search icon
		moveToElementClick(By.cssSelector("div[data-mode='allCourses'] label.btn-search"));
	}
	
	
	//click on bubble menu
	public void bubbleMenu()
	{
		if(driver.findElements(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses")).size()>0)
		{
			//wait for overlay to disappear
			invisibiltyOfElement(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses"));
		}
		//move the cursor on the course
		moveToElement(By.cssSelector(".box-img.loaded [href='/courseDetails/633']"));
		
		//move the cursor and click
		moveToElementClick(By.cssSelector("[ng-show=\"model.tab=='allCourses'\"] .clui-list-courses .clui-course .box-actions"));
	}
	
	//click on course insight option
	public void courseInsightCircle()
	{
		if(driver.findElements(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses")).size()>0)
		{
		//wait for overlay to disappear
		invisibiltyOfElement(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses"));
		}
		
		//move the cursor on the course
		//moveToElement(By.xpath("//div[@class='box-img loaded']//a[@class='box-link']"));
		moveToElement(By.cssSelector("[ng-show=\"model.tab=='allCourses'\"] .clui-list-courses .clui-course"));

		//move the cursor and click
		moveToElementClick(By.cssSelector("[ng-show=\"model.tab=='allCourses'\"] .clui-list-courses .clui-course .material-icons.md-21.mdi.position-absolute-center.mdi-account-search"));
				
		//wait for the course insight overlay to disappear
		invisibiltyOfElement(By.cssSelector(".section-content .clui-overlay-loading.alpha"));
				
	}
	
	//click on edit course option
	public void editCourseCircle()
	{
		//wait for overlay to disappear
		invisibiltyOfElement(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses"));
		
		//move the cursor on the course
		moveToElement(By.xpath("//div[@class='box-img loaded']//a[@class='box-link']"));
		
		//move the cursor and click
		moveToElementClick(By.cssSelector("[ng-show=\"model.tab=='allCourses'\"] .clui-list-courses .clui-course .clui-button .material-icons.md-21.mdi.position-absolute-center.mdi-pencil"));
				
		//wait for the course insight overlay to disappear
		invisibiltyOfElement(By.cssSelector(".section-content .clui-overlay-loading.light"));
				
	}
	

	//click on edit course option
	public void viewCourse()
	{
		//wait for overlay to disappear
		invisibiltyOfElement(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses"));
		
		//move the cursor on the course
		moveToElement(By.xpath("//div[@class='box-img loaded']//a[@class='box-link']"));
		
		//move the cursor and click
		moveToElementClick(By.cssSelector("[ng-show=\"model.tab=='allCourses'\"] .clui-list-courses .clui-course"));
				
		//wait for the course insight overlay to disappear
		invisibiltyOfElement(By.cssSelector(".section-content .clui-overlay-loading.light"));
				
	}
	
	//assign course
	public void clickAssignCourseOption() throws InterruptedException
	{
		//wait assign option to get appear
		visibilityOfElement(By.cssSelector(".ui-info-bubble-info.hover a[data-open-modal='modal-invite-new']"));
		
		//click on assign option
		moveToElementClick(By.cssSelector(".ui-info-bubble-info.hover a[data-open-modal='modal-invite-new']"));
		
		//add small wait
		Thread.sleep(2000);
		
		//locate the email field
		visibilityOfElement(By.id("emailInput"));
	}
	
	
	
	//add user to the invite list
	public void addUser(String cellValue) throws InterruptedException
	{
		writeText(By.id("emailInput"),  cellValue);
		//wait for a while 
		Thread.sleep(4000);
		
		moveToElementClick(By.cssSelector("button[type='submit']"));
		
		log.info("Added user to invite list-" + cellValue);
		
		//wait for a while 
		Thread.sleep(4000);
	}
	
	//uncheck the email user link
	public void untickEmailLink()
	{
		boolean emailToUserChkBox=driver.findElement(By.id("emailUser")).isSelected();
		if(emailToUserChkBox)
		{
			log.info("Email to user check box is pre selected");
			moveToElementClick(By.id("emailUser"));
			log.info("Email to user check box is now unselected");
		}
		
	}
	
	
	//assign course modal
	public void sendInvite() throws Exception
	{
		
		
		
		//click on send button
		moveToElementClick(By.xpath("//span[contains(text(),'Continue')]"));
		
		//wait for overlay to disappear
		invisibiltyOfElement(By.cssSelector("div[data-mode='allCourses'] .clui-overlay-loading.courses"));
		
		
	}
	
	//capture the close icon of invite success message
		public String successAlert()
		{
			//wait for the alert success message
			visibilityOfElement(By.cssSelector(".box-alert.SUCCESS"));
			
			String successNotification= readText(By.cssSelector(".box-alert.SUCCESS"));
			
			return successNotification;
			
		}

	//create course 
	public void createCourse()
	{
		//click on create course button
		moveToElementClick(By.id("clui-create-course-button"));
		
		//wait for overlay to disappear 
		invisibiltyOfElement(By.cssSelector("div[data-id='modal-course-create'].clui-modal>.clui-overlay-loading"));	
		
		
		courseDetails(); // enter all the valid details of course
		
		moveToElementClick(By.cssSelector("input[type='submit']")); //click on create button create new course modal
		
		//By addsection = By.cssSelector("div[class='section-title']");
		//ArrayList<WebElement> allElements = new ArrayList<WebElement>();
		//allElements.add(e)
		
		wait.until(ExpectedConditions.elementToBeClickable(addSectionElement()));
		
		
	}
	
	public By addSectionElement()
	{
		return By.cssSelector("div[class='section-title']");
	}
	
	public void courseDetails()
	{
		//move to element and enter course name
		writeText(By.id("title"), readFromDataFile( "createCourseName"));
		
	}
	
}
