package clui;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;




import org.openqa.selenium.interactions.Actions;


public class EditCoursePage extends BasePage{

	public static Logger log= LogManager.getLogger(EditCoursePage.class.getName());
	
	public EditCoursePage(WebDriver driver, WebDriverWait wait, Actions action, Properties prop) 
	{
		super(driver, wait, action,prop);
		
	}


	public void addSection(String sectionName) {
		
		
		
		//click on add section module
		moveToElementClick(By.cssSelector("div[class='section-title']"));
		
		visibilityOfElement(By.cssSelector(".box-widget-modal input[type='text']"));
		
		//write the section name
		writeText(By.cssSelector(".box-widget-modal input[type='text']"), sectionName);

		//click on submit button
		moveToElementClick(By.cssSelector(".box-widget-modal input[type='Submit']"));
		
	}
	
	
	
	
}
