package clui;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class ErrorStatePage extends BasePage{

	public static Logger log= LogManager.getLogger(ErrorStatePage.class.getName());
	
			public ErrorStatePage(WebDriver driver, WebDriverWait wait, Actions action, Properties prop)
			{
				super(driver, wait, action, prop);
				
			}
			
			//web elements
			
			//go back to all course page
			public By overlayHideCourse()
			{
				
				return By.cssSelector(".clui-overlay-loading.courses.absolute.ng-hide");
				
			}
			
			//go back to people page
			public By overlayHideUsers()
			{
				
				return By.cssSelector(".clui-overlay-loading.alpha.ng-hide");
				
			}
			
			
			
			//return header
			public By stateName() {
				
				return By.cssSelector(".font-size-24.color-blue");
				
			}
			
			//return description of error state
			public By errorDescription() {
				
				return By.cssSelector(".margin-bottom-30.color-navy");
				
			}
			
			
			//link to return back to previous page
			public By buttonCTA() {
				
				return By.cssSelector(".box-section .ui-button");
				
			}
			
			
			//go back to respective page
			public void clickCTA() throws InterruptedException
			{
				moveToElementClick(buttonCTA());
				
			}
			
		
			
			
}
