package clui;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import utils.ExcelUtil;

import org.openqa.selenium.interactions.Actions;


public class CourseInsightPage2 extends BasePage{

	public static Logger log= LogManager.getLogger(CourseInsightPage2.class.getName());
	
	public CourseInsightPage2(WebDriver driver, WebDriverWait wait, Actions action, Properties prop) 
	{
		super(driver, wait, action,prop);
		
	}

 
	
	public void searchUser(String cellValue) throws InterruptedException
	{
		
		//wait for search box to appear on course insight page
		elementToBeClickable(By.cssSelector("input[id='text-query']"));
		
		//click on search box first
		moveToElementClick(By.cssSelector("input[id='text-query']"));
		
		//pass the user name to search box and press enter
		writeText(By.cssSelector("input[id='text-query']"), cellValue);
		pressKey(By.cssSelector("input[id='text-query']"), Keys.ENTER );
		
		
		//wait for the following element to disappear
		invisibiltyOfElement(By.cssSelector(".section-content .clui-overlay-loading.alpha"));
		
		//add small wait
		Thread.sleep(2000);
		
	}
	
	public By emailOfSearchedUser(String cellValue)
	{
		return By.xpath("//a[contains(text(),'" + cellValue + "')]");
	}
	
	public void removeUser() throws InterruptedException
	{
		//add small wait
		Thread.sleep(2000);
		
		//click on three dot menu
		moveToElementClick(By.cssSelector(".cursor-pointer .bubble-icon .material-icons"));
		
		//add small wait
		Thread.sleep(3000);
		
		//wait for the enenroll button to appear
		visibilityOfElement(By.cssSelector(".box-description a[ng-click='removeEnrolment(item)']"));
				
		
		//Click on unenrol user option
		moveToElementClick(By.cssSelector(".box-description a[ng-click='removeEnrolment(item)']"));
		
		//add small wait
		Thread.sleep(2000);
		
		//wait for the element to appear
		visibilityOfElement(By.cssSelector("a[ng-click='confirm()']"));
		
		//add small wait
		Thread.sleep(2000);
		
		//confirm by clicking on remove button
		moveToElementClick(By.cssSelector("a[ng-click='confirm()']"));
		
		//wait for the overlay on course insight page to disappear
		invisibiltyOfElement(By.cssSelector(".section-content .clui-overlay-loading.alpha"));
				
		
	}

	//edit enrolment status 
	public void editEnrolementStatus(String status) throws InterruptedException
	{
		//add small wait
		Thread.sleep(2000);
		
		//click on three dot menu
		moveToElementClick(By.cssSelector(".cursor-pointer .bubble-icon .material-icons"));
		
		//add small wait
		Thread.sleep(3000);
		
		//wait for the enenroll button to appear
		visibilityOfElement(By.cssSelector(".bubble-info.ui-info-bubble-info.dropdown.hover"));
				
		//add small wait
		Thread.sleep(3000);
		
		log.info("Click on edit enrolment option ");
		
		//click edit enrolment option
		selectListOption(By.cssSelector(".bubble-info.ui-info-bubble-info.dropdown.hover .menu-actions li"), "Edit enrolment");
		
		
		//wait for edit enrolment modal to appear completely
		visibilityOfElement(By.cssSelector(".box-content .button-close-modal"));
		Thread.sleep(3000);
	
		//click on enrolment status dropdown
		moveToElementClick(By.cssSelector(".box-content .lbl-select.ui-button.fullwidth.hover"));
		
		Thread.sleep(3000);
		//wait for enrolment status drop down to appear completely
		visibilityOfElement(By.cssSelector("body>.box-select-options"));
		
		log.info("Select option to reset enrolment ");
		//click reset enrolment option
		selectListOption(By.cssSelector("body>.box-select-options li"), status);
				
		Thread.sleep(2000);
		
		//Save the changes by clicking on save changes button
		moveToElementClick(By.id("set-enrolment-submit"));
		
		//wait for confirmation modal to appear completely
		visibilityOfElement(By.cssSelector(".box-content .clui-panel"));
		Thread.sleep(3000);
		
		//Confirm the changes by clicking on reset course progress
		moveToElementClick(By.cssSelector("a[ng-click='confirm()']"));
		
		//wait for the overlay on course insight page to disappear
		invisibiltyOfElement(By.cssSelector(".clui-overlay-loading"));
				
		
	}

	

	public void clearFilter() throws InterruptedException 
	{
		//add small wait
		Thread.sleep(2000);
				
		//clear the search for next iteration
		moveToElementClick(By.cssSelector("a[ng-click='clearFilter()']"));
				
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
