package clui;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage{

	public static Logger log= LogManager.getLogger(HomePage.class.getName());
	
	//constructor
	public HomePage(WebDriver driver, WebDriverWait wait, Actions action, Properties prop) 
	{
		super(driver, wait, action, prop);
	}
	



	//page variables
	String baseURL= "https://qa.clui.com/courses";
	
	//Web elements--

	
	//page methods
	
	//go to landing page
	public void goToQA()
	{
		driver.get(baseURL);
	}

	//click on sign-in option
	public void goToCredentials() 
	{
		visibilityOfElement(loginLink());
		moveToElementClick(loginLink());
		
	}
	
	public By loginLink()
	{
		return By.id("clui-login-button");
		
	}
}
