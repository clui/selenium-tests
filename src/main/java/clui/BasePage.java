package clui;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {
	public WebDriver driver;
	public WebDriverWait wait;
	public Actions action;
	public Properties prop;
	
	public static Logger log= LogManager.getLogger(BasePage.class.getName());
	//Constructor
    public BasePage (WebDriver driver, WebDriverWait wait, Actions action, Properties prop)
    {
        this.driver = driver;
        this.wait = wait;
        this.action= action;
        this.prop= prop;
    }
    
   

	//click method
    public void click(By elementlocation)
    {
    	driver.findElement(elementlocation).click();
    }
    
    //write text
    public void writeText(By elementlocation, String text)
    {
    	driver.findElement(elementlocation).clear();
    	driver.findElement(elementlocation).sendKeys(text);
    	
    }
    
    //press any key
    public void pressKey(By elementlocation, Keys pressbutton)
    {
    	driver.findElement(elementlocation).sendKeys(pressbutton);
    }
    
    //read text
    public String readText(By elementlocation)
    {
    	return driver.findElement(elementlocation).getText().trim();
    }
    
    //check whether the element is visible on page
    public Boolean visibleElement(By elementlocation)
    {
    	return driver.findElement(elementlocation).isDisplayed();
    	
    }
    
    
    //wait for visibility of that element 
    public void visibilityOfElement(By elementlocation)
    {
    	
    	wait.until(ExpectedConditions.visibilityOfElementLocated(elementlocation));
    }
    
    //wait for invisibility of all elements --------------------------------------------- fix this tuestday
    public void invisibilityOfElements( WebElement element)
    {
    	
    	wait.until(ExpectedConditions.invisibilityOfAllElements(element));
    }
    
  //wait for invisibility of that element 
    public void invisibiltyOfElement(By elementlocation)
    {
    	wait.until(ExpectedConditions.invisibilityOfElementLocated(elementlocation));
 
    }
    
  //wait for that element to be click able 
    public void elementToBeClickable(By elementlocation)
    {
    	wait.until(ExpectedConditions.elementToBeClickable(elementlocation));
    }
    
    
    
    //move mouse on the middle of element
    public void moveToElement(By elementlocation)
    {
    	action.moveToElement(driver.findElement(elementlocation)).perform();
    }
    
    //move mouse on the middle of element and click
    public void moveToElementClick(By elementlocation)
    {
    	action.moveToElement(driver.findElement(elementlocation)).click().perform();
    }
    
   
    
    //get value from data file
   public String readFromDataFile(String textData) 
   {
	 return prop.getProperty(textData);
   }
   
   
   //select todays date from date picker
   public void pickTodayDate(By elementTodayDate,By elementBackToToday ,By elementDateClear, By elementDateDone)
   {
		wait.until(ExpectedConditions.visibilityOfElementLocated(elementDateClear));
		moveToElementClick(elementDateClear);
       moveToElementClick(elementBackToToday);
		moveToElementClick(elementTodayDate);
		moveToElementClick(elementDateDone);
   }
   
   //select option from the drop down list
   public void selectListOption(By listElementLocation, String optionText)
   {
	   List<WebElement> opt= driver.findElements(listElementLocation);
		
		for(WebElement options:opt)
		{
			if(options.getText().trim().equals(optionText))
			{
				log.info("Clicking the option from drop down list - "+ options.getText());
				options.click();
				break;
			}
		}
   }
   
   //broken link from page
   public void brokenLink(String homePage)
   {
	   String url= "";
	   HttpURLConnection huc = null;
	   int respCode = 200;
	   
	   List<WebElement> links = driver.findElements(By.tagName("a"));
       
       Iterator<WebElement> it = links.iterator();
       
       
       while(it.hasNext()){
           
           url = it.next().getAttribute("href");
           String elementLocation= it.next().getLocation().toString();
           
           
           log.info("url captured- " + url);
       
           if(url == null || url.isEmpty()){
        	   log.info("URL is either not configured for anchor tag or it is empty");
        	   log.info("********************************Element location- "+ elementLocation);
               continue;
           }
           
           if(!url.startsWith(homePage)){
               log.info("URL belongs to another domain, skipping it.");
               continue;
           }
           
           try {
               huc = (HttpURLConnection)(new URL(url).openConnection());
               
               huc.setRequestMethod("HEAD");
               
               huc.connect();
               
               respCode = huc.getResponseCode();
               
               log.info("the response code is - "+ respCode);
               
               if(respCode >= 400){
                   log.info("********************"+ url+" is a broken link*****************************************************");
               }
               else{
                   log.info(url+" is a valid link");
               }
                   
           } catch (MalformedURLException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
           } catch (IOException e) {
               // TODO Auto-generated catch block
               e.printStackTrace();
           }
       }
   }
   
}
