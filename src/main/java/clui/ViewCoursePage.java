package clui;

import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.ExcelUtil;

import org.openqa.selenium.support.ui.ExpectedConditions;






import org.openqa.selenium.interactions.Actions;


public class ViewCoursePage extends BasePage{

	public static Logger log= LogManager.getLogger(ViewCoursePage.class.getName());
	
	public ViewCoursePage(WebDriver driver, WebDriverWait wait, Actions action, Properties prop) 
	{
		super(driver, wait, action,prop);
		
	}

	//return course status  element
		public By courseStatusElement() {
			
			return By.cssSelector(".box-progress .clui-badge-status");
			
		}
		
		//return path section element 
		public By pathSectionElement() {
			
			return By.cssSelector(".list-section li.section:nth-child(2) .section-title");
			
		}
	
		
		//return sign frame
		public By signFrameElement()
		{
			return By.cssSelector(".box-input .signature-pad");
		}
		
	//return enrol button element
	public By enrolButtonElement() {
		
		return By.id("box-action-enrol");
		
	}
	
	//return purchase button element
	public By purchaseButtonElement() {
		
		return By.cssSelector("[id='box-action-purchase'] .button-purchase .box-price");
		
	}
	
	//return enrol button element
		public By unEnrolButtonElement() {
			
			return By.id("box-action-unenrol");
			
		}
	
	//return launch button element
		public By launchButton() {
			
			return By.cssSelector(".box-action .blue.ui-button .lbl-button");
			
		}
	
		// return review modal
	public By reviewModal() {
			
			return By.cssSelector(".box-content [ng-submit='submitForm()']");
			
		}
	
	// return close modal of form
		public By closeFormModal()
		{
			return By.cssSelector(".button-close-modal .material-icons.mdi-close");
		}
		
		// return element today date css
		public By elementDateToday()
		{
				
			return By.cssSelector(".ui-calendar-layer.focus .box-calendar [title*='Today']");
				
		}	
		
		// return element clear date css
		public By elementDateClear()
		{
				
			return By.cssSelector(".ui-calendar-layer.focus .box-calendar  [ng-click='clear()']");
				
		}

		// return element for back to today
		public By elementBackToToday()
		{

			return By.cssSelector(".ui-calendar-layer.focus .box-calendar  [ng-click='backToToday()']");

		}
		
		// return element clear date css
		public By elementDateDone() 
		{
						
			return By.cssSelector(".ui-calendar-layer.focus .box-calendar .small.blue.ui-button");
		}
				
		//return option drop down css
		public By formOptionDropdown() 
		{
						
			return By.cssSelector(" body>.box-select-options .list-select-options li");
		}		
		
		//return the label/selection element of selected card drop down
		public By selectCardLabel()
		{
			//store the text in selected dropdown of card type
			return By.cssSelector(".ui-select-selectedCard .label");
		}
		
		//return purchase success message text
		public By purchaseSuccessMessage() {
			return By.xpath("//h2[contains(text(),'Purchase successful!')]");
		}
		
		/*---------------Elements related to payment modal------------------------------------------*/
		
		//return selectcard element dropdown
		
	//click on launch button
		public void openModal(By elementlocation)
		{
			moveToElementClick(elementlocation);
			invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.dark.ui-overlay-loading"));
		}
		
		
	//enroll the course
	public void enrollCourse() throws InterruptedException
	{
		try {
		moveToElementClick(enrolButtonElement());
		invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.alpha"));
		}
		catch (TimeoutException toe)
		{
			log.info("Exception caught and handled- " + toe);
			log.info("Still continue the test --");
		}
		//wait for a while 
		Thread.sleep(5000);
	}
	
	
	//purchase course
	public void clickPurchaseCourse() throws InterruptedException
	{
		moveToElementClick(purchaseButtonElement());
		visibilityOfElement(By.cssSelector(".box-widget-modal .box-content"));
		
		//wait for a while 
		Thread.sleep(5000);
		
		//click on continue to payment 
		moveToElementClick(By.cssSelector(".box-widget-modal .box-content .blue.ui-button"));
		
		
		//wait for submit order button to appear
		elementToBeClickable(By.id("newCreditCard-submit"));
		
		//wait for select card option to appear
		visibilityOfElement(selectCardLabel());
				
	}
	
	
	
	//purchase course
		public void clickPurchaseCourseOthers() throws Exception
		{
			moveToElementClick(purchaseButtonElement());
			visibilityOfElement(By.cssSelector(".box-widget-modal .box-content"));
			
			//wait for a while 
			Thread.sleep(5000);
			
			////click on purchase for others
			moveToElementClick(By.cssSelector(".box-section .mdi-account-multiple"));
			
			
			//click on continue to payment to check if it is disabled or not
			visibilityOfElement(By.cssSelector(".box-widget-modal .box-content .blue.ui-button.disabled"));
			moveToElementClick(By.cssSelector(".box-widget-modal .box-content .blue.ui-button.disabled"));
			
			//nothing should happen after clicking the continue to payment option
			//add user to purchase list
			addUserPurchaseList();
			
			log.info("users are added to the purchase list");
			
			//wait for a while 
			Thread.sleep(3000);
			
			//click on continue to payment 
			moveToElementClick(By.cssSelector(".box-widget-modal .box-content .blue.ui-button"));
			
			//wait for submit order button to appear
			elementToBeClickable(By.id("newCreditCard-submit"));
			
			//wait for select card option to appear
			visibilityOfElement(selectCardLabel());
					
		}
		
		//user purchase list
		public void addUserPurchaseList() throws Exception
		{
			//Set Test Data Excel and Sheet
			ExcelUtil.setExcelFileSheet("purchaseUserList");
			
			int userNum=1;
			
			String inviteUser = ExcelUtil.getCellData(userNum, 1);
			String userType = ExcelUtil.getCellData(userNum, 0);
			
			while(inviteUser != null) 
			{
				writeText(By.cssSelector("[name='email']"), inviteUser);
				//wait for a while 
				Thread.sleep(2000);
				
				moveToElementClick(By.cssSelector("[value='Add user']"));
				if(userType.equalsIgnoreCase("invalid"))
				{
					visibilityOfElement(By.cssSelector(".ui-input-warning [ng-message='email']"));
					log.info("Front End validation displaying for invalid email below the email field - "+ readText(By.cssSelector(".ui-input-warning [ng-message='email']")) );
				}
				else if(userType.equalsIgnoreCase("duplicate"))
				{
					
					visibilityOfElement(By.cssSelector(".ui-input-warning [ng-if='purchaseInfo.emailDuplicated']"));
					log.info("Front End validation displaying for duplicate email below the email field  - "+ readText(By.cssSelector(".ui-input-warning [ng-if='purchaseInfo.emailDuplicated']")) );

				}
				else if(userType.equalsIgnoreCase("valid"))
				{
					//wait for a while 
					Thread.sleep(4000);
					
					if(driver.findElements(By.xpath("//div[contains(text(),'"+ inviteUser + "')]")).size() >0)
					{
						
						log.info("The user- " + inviteUser +" is added to purchase list");
					}
				}
				
				//increment the row number
				userNum++;
				
				//check to return only not null values
				if(ExcelUtil.getCellData(userNum, 1)!= null) 
				{
					inviteUser=ExcelUtil.getCellData(userNum, 1);
					userType=ExcelUtil.getCellData(userNum, 0);
				}
				else 
				{
					inviteUser=null;
				}
			}
		}
		
	
	//displays the alert message
	public void alertMessage() throws InterruptedException
	{
		visibilityOfElement(By.cssSelector(".box-alert"));
		log.info("The alert message- "+ readText(By.cssSelector(".box-alert")));
		invisibiltyOfElement(By.cssSelector(".box-alert"));
		
		Thread.sleep(5000);
		
	}
	
	
	//enter the card details and submit the order
	public void purchaseCard(String cardType, String cardHolderName, String cardNumber, String expMonth, String expYear, String cvv) throws InterruptedException
	{
		log.info("Enter card details");
		
		if(cardType.contains("Card ending in"))
		{
			log.info("Have a saved card detail");
			moveToElement(selectCardLabel());
			selectListOption(By.cssSelector(".list-select-options li"), "Use a new card" );
			log.info("changed selection to - use a new card ");
		}
		else if(cardType.contains("Use a new card"))
		{
			log.info("No saved card detail");
			
		}
		 
			Thread.sleep(5000);
			
			writeText(By.cssSelector(".box-section [name='cardHolderName']"), cardHolderName );
			writeText(By.cssSelector(".box-section [name='cardNo']"), cardNumber );
			writeText(By.cssSelector(".box-section [name='cvc']"), cvv);
			
			
			//Select expiry month
			moveToElementClick(By.cssSelector(".ui-select.ui-select-expMonth .lbl-select"));
			selectListOption(By.cssSelector("body>.box-select-options .list-select-options li"), expMonth);
			
			
			//Select expiry year
			moveToElementClick(By.cssSelector(".ui-select.ui-select-expYear .lbl-select"));
			selectListOption(By.cssSelector("body>.box-select-options .list-select-options li"), expYear);
			
			
			moveToElementClick(By.id("newCreditCard-submit"));
		
		
		invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.dark.ui-overlay-loading"));
		
	}
	
	//close the purchase success modal
	public void viewCourseAction() throws InterruptedException
	{
		moveToElementClick(By.xpath("//span[contains(text(),'View course')]"));
		try {
			invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.alpha"));
			}
			catch (TimeoutException toe)
			{
				log.info("Exception caught and handled- " + toe);
				log.info("Continue to course insight page --");
			}
			//wait for a while 
			Thread.sleep(5000);
	}
	
	//enter the details in form1
		public void formAutoAssessment() throws InterruptedException
		{
			//wait for element to be enable before interacting
			visibilityOfElement(By.cssSelector(".type-text .ui-input-text"));
			
			//fill name text field
			writeText(By.cssSelector(".type-text .ui-input-text"), "This is data for auto assessment form module");
		}
	
	//enter the details in form1
	public void formDetails() throws InterruptedException
	{
		//wait for element to be enable before interacting
		visibilityOfElement(By.cssSelector(".type-text .ui-input-text"));
		
		//fill name text field
		writeText(By.cssSelector(".type-text .ui-input-text"), "test form data");
		
		//WebElement signFrame= driver.findElement(signFrameElement());
		
		/* Keeping this for future as cannot validate the result
		 
		  Thread.sleep(2000);
		
		//draw the signature
		action.moveToElement(signFrame, 10, 10)
						.clickAndHold()
						.moveByOffset(20, 20)
						.moveByOffset(30, 30)
						.moveByOffset(40, 40)
						.moveByOffset(50, 50)
						.moveByOffset(60, 60)
						.moveByOffset(70, 70)
						.build().perform();
		
		Thread.sleep(2000);
		
		action.release().perform();
		
		Thread.sleep(2000);
		
		*/
		
		//click all the three radio buttons for age
		moveToElementClick(By.cssSelector("[for='field_field_1243_0'] .material-icons.mdi-radiobox-blank ")); 
		moveToElementClick(By.cssSelector("[for='field_field_1243_1'] .material-icons.mdi-radiobox-blank "));
		moveToElementClick(By.cssSelector("[for='field_field_1243_2'] .material-icons.mdi-radiobox-blank "));
		

		Thread.sleep(2000);  
		
		
		//Select options
		moveToElementClick(By.cssSelector(".ui-select-field_1244 .lbl-select"));
		selectListOption(formOptionDropdown(), "arts");
		
		
		Thread.sleep(2000);
		
		//click on date element and select current date
		moveToElementClick(By.cssSelector("[data-name='field_1245']"));
		pickTodayDate(elementDateToday(),elementBackToToday(), elementDateClear(), elementDateDone());
		
		Thread.sleep(2000);
		
		//select rating
		moveToElementClick(By.cssSelector("[data-id='field_1246'] label:nth-child(10)"));
		
		
	}
	
	
	//enter the details in state form
		public void stateFormDetails() throws InterruptedException
		{
			//wait for element to be enable before interacting
			visibilityOfElement(By.cssSelector(".box-section .ui-input-textarea"));
			
			Thread.sleep(3000);
			
			//fill name text field
			writeText(By.cssSelector(".box-section .ui-input-textarea"), "Entering random data to selected state form");
			
			Thread.sleep(3000);
		}
	//send for approval
		public void sendForApproval() throws InterruptedException
		{
			//click on send for approval button
			moveToElementClick(By.cssSelector("div[ng-if=\"moduleEnrolment.type=='ApprovalModule' && !readOnly\"] >a"));
			
			//wait for modal to open
			visibilityOfElement(By.cssSelector(".button-close-modal"));
			
			Thread.sleep(3000);
			
			//click on submit progress button
			moveToElementClick(By.cssSelector(".box-content a[ng-click='markApprovalAsInProgress()']"));
			
			
			//wait for the overlay to disappear
			invisibiltyOfElement(By.cssSelector(".clui-overlay-loading"));
			
			
		}
		
	//approval success message
		public String messageSubmitApproval()
		{
			visibilityOfElement(By.cssSelector("div[class='box-message']"));
			String successMessage= readText(By.cssSelector("div[class='box-message']"));
			
			return successMessage;
			
		}
		
	//submit the modal
	public void submitModule() throws InterruptedException
	{
		//click on submit button
		moveToElementClick(By.cssSelector("[data-id='modal-module-form']  input[type='submit']"));
		invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.alpha"));
		
		Thread.sleep(3000);
	}
	
	//select path module and save the selection
		public void pathModule(String pathName) throws InterruptedException
		{
			//wait for element to be enable before interacting
			visibilityOfElement(By.cssSelector(".ui-input-radio "));
			
			//select the path option 
			selectListOption(By.cssSelector(".ui-input-radio label"), pathName);
			
			//save the selection 
			moveToElementClick(By.cssSelector(".box-content [click-action='save()'] [value='Save']"));
			
			invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.alpha"));
			
			Thread.sleep(3000);
			
		}
	
	//quiz module
	public void quizModule(String quizAction) throws InterruptedException {
		
		if(quizAction=="Get Started"){
		log.info("Starting the quiz");
		//launch quiz module
		moveToElementClick(By.cssSelector(".box-action .blue.ui-button .lbl-button"));
		visibilityOfElement(By.xpath("//a[contains(text(),'Get Started')]"));
		
		
		//click on get started link to start with quiz
		moveToElementClick(By.xpath("//a[contains(text(),'Get Started')]")); 
		
		Thread.sleep(5000);
		}
		
		if(quizAction=="Submit Quiz")
		{
			log.info("Submit the quiz");
			//Submit quiz module
			visibilityOfElement(By.xpath("//a[contains(text(),'Submit Quiz')]"));
			moveToElementClick(By.xpath("//a[contains(text(),'Submit Quiz')]"));
			Thread.sleep(5000);
			
			log.info("Score printed on screen- "+ readText(By.xpath("//h3[contains(text(),'You scored 3 out of 3!')]")));
			
			moveToElementClick(By.xpath("//div[@class='box-buttons']//a[@class='ui-button gogreen'][contains(text(),'Next Module')]"));
			invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.alpha"));
			Thread.sleep(5000);
			
		}
		
	}
	
	//quiz answers
	public void quizAnswers(String quizAnswer) throws InterruptedException {
	
		if(quizAnswer== "Beer")
		{
			moveToElementClick(By.xpath("//checkbox-label[contains(text(),'Beer')]"));
			Thread.sleep(5000);
			
		}
		if(quizAnswer== "Hulk")
		{
			moveToElementClick(By.xpath("//checkbox-label[contains(text(),'Hulk')]"));
			Thread.sleep(5000);
		}
		if(quizAnswer== "Joker")
		{
			moveToElementClick(By.xpath("//checkbox-label[contains(text(),'Joker')]"));
			Thread.sleep(5000);
		}
		
	}
	
	
	
	//write review for that course
	public void review() throws InterruptedException
	{
		visibilityOfElement(By.cssSelector(".box-content [data-id='review']"));
		//writeText(By.cssSelector(".box-content [data-id='review'] .ui-input-textarea"), "The course is amazing, giving 5 stars");
		//moveToElementClick(By.cssSelector(".box-content .yellow.display-inline-block [value='Save review']"));
		
		//wait for skip button to be click able 
		elementToBeClickable(By.cssSelector(".box-content .outline.ui-button .lbl-button"));
		//not giving review just simply clicking skip
		moveToElementClick(By.cssSelector(".box-content .outline.ui-button .lbl-button"));
		invisibiltyOfElement(By.cssSelector(".clui-overlay-loading.alpha"));
		
		Thread.sleep(5000);
		
	}
	
	//navigate to course insight page
	public void courseInsight()
	{
		moveToElementClick(By.cssSelector(".disableAnimate .outline.ui-button .mdi-account-search"));
		//wait for the course insight overlay to disappear
		invisibiltyOfElement(By.cssSelector(".section-content .clui-overlay-loading.alpha"));
		
	}
	
}
