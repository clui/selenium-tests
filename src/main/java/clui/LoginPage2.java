package clui;

import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class LoginPage2 extends BasePage{

	public static Logger log= LogManager.getLogger(LoginPage2.class.getName());
	
			public LoginPage2(WebDriver driver, WebDriverWait wait, Actions action, Properties prop)
			{
				super(driver, wait, action, prop);
				
			}
			
			//web elements
			//return element of user logout option
			public By userLogoutOption() 
			{
							
				return By.id("clui-username-label");
			}
			
			public String testType(XSSFRow row) 
			{
				String typeOfTest = row.getCell(0).toString();
				return typeOfTest;
			}
			
			//login to qa.clui
			public void loginQA(XSSFRow row) 
			{
				//wait for the email field to appear
				visibilityOfElement(By.cssSelector("input[name='email'],input[name='password']"));
				
				//Enter username(email)
				writeText(By.cssSelector("input[name='email']"), row.getCell(1, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).toString());
				
				//Enter password
				writeText(By.cssSelector("input[name='password']"), row.getCell(2,Row.MissingCellPolicy.CREATE_NULL_AS_BLANK).toString());
				
				//click on login button
				moveToElementClick(By.id("login-submit"));
			}
			
			//logout to qa.clui
			public void logoutQA() throws InterruptedException
			{
				//wait for user name label to appear
				elementToBeClickable(userLogoutOption());
				boolean logoutSuccess= false;
				
				do {
					
				//click on username label
				moveToElementClick(userLogoutOption());
				
				//handle timeout exception for log out button
				try {
					
				//wait for logout button to appear
				visibilityOfElement(By.id("clui-logout-button"));
		
				//click on logout button
				moveToElementClick(By.id("clui-logout-button"));
				}
				catch(TimeoutException toe)
				{
					log.info("Exception caught and handled- " + toe);
					log.info("Try again to log out");
					logoutSuccess=false;
					continue;
				}
				
				//set logout true and exit the loop
				logoutSuccess=true; 
				
				} while(!logoutSuccess);
	
				log.info("Successfully logged out");
				//wait for logout
				Thread.sleep(2000);
			}
			
			
			//Wrong user name pop up
			//validate the message for invalid user
			public String messageInvalidUser()
			{
				visibilityOfElement(By.cssSelector("div[class='box-message']"));
				String errorNotificationHeader= readText(By.cssSelector("div[class='box-message']"));
				
				return errorNotificationHeader;
				
			}
			
			//validation for required fields
			public String frontendValidation()
			{
				visibilityOfElement(By.cssSelector("[ng-message='required']"));
				String validationError= readText(By.cssSelector("[ng-message='required']"));
				
				return validationError;
				
			}
			
			//invalid password message validation
			public String messageInvalidPassword()
			{
				visibilityOfElement(By.cssSelector("div[class='clui-col'] .box-title"));
				String errorNotificationHeader= readText(By.cssSelector("div[class='clui-col'] .box-title"));
				
				return errorNotificationHeader;
			}
			
			//validate the course overlay
			public void loadingCourses() throws InterruptedException
			{
				invisibiltyOfElement(By.cssSelector("[id='login-loading-overlay']"));
				Thread.sleep(4000);
			}
			
			
}
